/*
 * ANSI.cpp
 *
 * Created: 7/17/2018 12:04:34
 *  Author: Fredc
 */ 

  #include "NHD1286.h"
  #include "ANSI.h"
  #include "Test.h"

  #include  <stdio.h>

  #define CHAR_WIDTH 6     // Cols per char

  void ANSI::Init()
     {
     Ansi_state = 0;          // Start our state machine 
     NHD1286::Init ( );       // Reset display 
     NHD1286::Set_Col (0); 
     NHD1286::Set_Row (0); 
     }

  
  void ANSI::Clear ( )
    {
    NHD1286::Clear ( );
    }


  

  int ANSI::State ( )               // Return our state
     {
     return Ansi_state;
     }
  
  void ANSI::Put (char *ch)
     {
     //NHD1286::DispString (ch);
      while (*ch != '\0')
         {
         Put (*ch);
         ch += 1;
         }
     }

  void ANSI::Put (char ch)
     {
     char str [30];

     next_ANSI_state = 0;
     //sprintf (str, "\r\nState %d %x", Ansi_state, ch);
     //NHD1286::DispString (str);

     //NHD1286::DispChar (ch);

     switch (Ansi_state)
        {
        case 0: 
           switch (ch)
              {
              case '~':
              case ESC:  next_ANSI_state = 1;     break;
              case '\r': NHD1286::Set_Col (0);    break;
              case '\n': NHD1286::Set_Row (NHD1286::Get_Row ( ) + 1);  break;
              default:   NHD1286::DispChar (ch);  break;
              }
           break;

        case 1: 
          switch (ch)
             {
             case '[': next_ANSI_state = 2; accum1 = 0; accum2 = 0; break;

             case '1': Test::Test1 ( ); break;
             case '2': Test::Test2 ( ); break;
             case '3': Test::Test3 ( ); break;
             case '4': Test::Test4 ( ); break;
             case '5': Test::Test5 ( ); break;
             case '6': Test::Test6 ( ); break;
             case 'c': Clear ( );       break;    //Serial_Port::Send_Tx0 (ch);

             default:  next_ANSI_state = 0; break;
             }
           break;

        case 2:
            switch (ch)
               {
               case '0': case '1': case '2': case '3': case '4':
               case '5': case '6': case '7': case '8': case '9':
                  accum1 = accum1 * 10 + (ch - '0');
                  next_ANSI_state = 3;
                  break;
              case 's':           // Save cursor
                  break;
              case 'u':           // Restore Cursor
                 break;
              case 'K':           // Erase To Line
                 break;
             }
           break;

        case 3:
           switch (ch)
              {
              case '0': case '1': case '2': case '3': case '4':
              case '5': case '6': case '7': case '8': case '9':   
                 accum1 = accum1 * 10 + (ch - '0');
                 next_ANSI_state = 3;
                 break;
             
              case ';':
                 next_ANSI_state = 4;
                 break;

              case 'A':                // Cursor Up specified number of lines
                 NHD1286::Set_Row (NHD1286::Get_Row ( ) + accum1);
                 next_ANSI_state = 0;

                 //sprintf (str, "Set Row %d ", NHD1286::Get_Row ( ) );
                 //NHD1286::DispString (str);
                 //NHD1286::DispString ("ESC A ");
                 break;
              case 'B':               // Cursor Down  by the specified number of lines
                 NHD1286::Set_Row (NHD1286::Get_Row ( ) - accum1);
                 next_ANSI_state = 0;

                 //sprintf (str, "Set Row %d ", NHD1286::Get_Row ( ) );
                 //NHD1286::DispString (str);
                 //NHD1286::DispString ("ESC B ");
                 break;

              case 'C':             // Cursor Forward b
                 NHD1286::Set_Col (NHD1286::Get_Col ( ) + accum1 * CHAR_WIDTH);
                 next_ANSI_state = 0;

                  //sprintf (str, "Col %d ", NHD1286::Get_Col ( ) );
                  //NHD1286::DispString (str);
                 //NHD1286::DispString ("ESC C ");
                 break;

              case 'D':             // Cursor Backward
                 NHD1286::Set_Col (NHD1286::Get_Col ( ) - accum1 * CHAR_WIDTH);
                 next_ANSI_state = 0;

                 //sprintf (str, "Col %d ", NHD1286::Get_Col ( ) );
                 //NHD1286::DispString (str);
                 //NHD1286::DispString ("ESC D ");
                 break;
             case 'J':             // Clear Display if we see 2J
                 if (accum1 == 2) NHD1286::Clear ( );
                 next_ANSI_state = 0;
                 break;  
            }               
          break;


       case 4:
           switch (ch)
              {
               case '0': case '1': case '2': case '3': case '4':
               case '5': case '6': case '7': case '8': case '9':
                  accum2 = accum2 * 10 + (ch - '0');
                  next_ANSI_state = 4;
                  break;
                  
               case 'H':         //Moves the cursor to the specified position (coordinates).
               case 'f':
                  NHD1286::Set_Row (accum1);
                  NHD1286::Set_Col (accum2 * CHAR_WIDTH); 
                  next_ANSI_state = 0;

                  //sprintf (str, "Set Row Col %d %d", accum1, accum2);
                  //NHD1286::DispString (str);
                  break;
           
             // default:  NHD1286::DispString ("ESC def"); break;
              }
         // next_ANSI_state = 0;
          break;

        default: break;
        }

     Ansi_state = next_ANSI_state;
     }
        //Esc[Line;ColumnH                   Moves the cursor to the specified position (coordinates).
        //Esc[Line;Columnf 	                Moves the cursor to the specified position (coordinates).
        //                                   If you do not specify a position, the cursor moves (line 0, column 0).

        //Esc[ValueA 	         Cursor Up specified number of lines
        //Esc[ValueB 	         Cursor Down  by the specified number of lines
        //Esc[ValueC 	         Cursor Forward by the specified number of columns If the cursor is already in the rightmost column, ignores this sequence.
        //Esc[ValueD 	         Cursor Backward specified number of columns If the cursor is already in the leftmost column ignores this sequence.
        //Esc[s 	               Save Cursor Position:
        //Esc[u               	Restore Cursor Position:
        //Esc[2J 	            Erase Display moves the cursor to the home position (line 0, column 0).
        //Esc[K 	                Erase Line:
