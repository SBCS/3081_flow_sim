/*
 * NHD1286.cpp
 *
 * Created: 6/15/2018 13:47:21
 *  Author: Fredc
 */ 

   

   #define F_CPU 8000000UL
   #include <util/delay.h>
   #include <avr/io.h>

   #include "NHD1286.h"
   #include "Font.h"
   #include "Util.h"

   #define P31 0x01		// Move to port E (VI:60)
   #define P36 0x02		// move to port E (VI:60)
   #define P32 0x04		// Display reset
   #define P37 0x02     // Read /Write
   #define P30 0x01     // aka Register select
   #define P34 0x04		// The E strobe

   #define LEFT_PANEL  0
   #define RIGHT_PANEL 1
   
   int NHD1286::Active_Panel = LEFT_PANEL;  // 0 == left
   int NHD1286::Row = 0;					// Current Row
   int NHD1286::Col = 0;					// Current Column
   
   void NHD1286::Init (void)
     {                        
      DDRA = 0xff;            // Port A All out
      DDRB = 0x07;            // Control signals
      DDRE = 0x07;            // Reset line & CS 1 & 2
      
      Display_RST (true);     // This is P32
      Display_RST (false);
      
      PORTB &= ~P34;
      PORTE &= ~P30;
      PORTB &= ~P37;
      PORTB &= ~P36;
      PORTE &= ~P31;
      
      PORTA &= 0x01;
      DispCommand (0x3f, LEFT_PANEL);
      DispCommand (0x3f, RIGHT_PANEL);
	  Set_Row (0);
	  Set_Col (0);
      }
      
   void NHD1286::Display_RST (bool R)
      {
      if (R)  PORTE &= ~P32;
         else PORTE |= P32;
      }         
   //---------------------------------------------------------

   void NHD1286::Display_ON (bool N) 
      {
      if (N)
        {
        DispCommand(0x3f, LEFT_PANEL);
        DispCommand(0x3f, RIGHT_PANEL);
        }
        else
        {
        DispCommand(0x3e, LEFT_PANEL);
        DispCommand(0x3e, RIGHT_PANEL);
        }
     }
   int NHD1286::Get_Row (void)
      {
      return Row;
      }
   
   int NHD1286::Get_Col (void)
      {
      return Col;
      } 
         
  void NHD1286::Set_Row (int X)
     {
     int x;
     
     Row = X;
     x = X & 0x07;
     x |= 0xb8;

    if (Active_Panel == 0) DispCommand (x, LEFT_PANEL);
        else DispCommand (x, RIGHT_PANEL); 
      }
              
  void NHD1286::Set_Col (int Y)
    {
    int y;

    Col = Y;

    if (Y < 64) Active_Panel = LEFT_PANEL;
       else     Active_Panel = RIGHT_PANEL;
    y = Y & 0x3f;
    y |= 0x40;
    DispCommand (y, Active_Panel);   
    }
    
int NHD1286::Incr_Col (void)  
   {
   Col += 1;
   if (Col > 127)		// Room for another char?
      {
      Col = 0;
      Incr_Row ( );
      Set_Col (0);
      Set_Row (Row);
      }
   return Col;
   }   
   
int NHD1286::Incr_Row (void)
   {
	Row += 1;
	if (Row > 7)		// Room for another char?
	   {
	    Row = 7;
	   }
    Set_Row (Row);
	return Row;
   }   
   
void NHD1286::Set_Line (int L)
   {
   int y;
   
   y = L & 0x07;
   y |= 0xb8;
   DispCommand (y, Active_Panel); 
   }
 
 void NHD1286::Write (int W)
     {
     DispData (W);
     }
     
 int NHD1286::Read (void) 
     {
     return 0x55; 
     }
      
  void NHD1286::Clear (void)
     {
     int line;
     int col;

     for (line = 0; line < 8; line++)
       {
       for (col = 0; col < 128; col++)
          {
          Set_Col (col);
          Set_Line (line);
          DispData (0);
          }
       }
    
      Display_ON (true);
     }
          
 int NHD1286::Status (void) 
     {
     return 0xaa;   
     }
   
 void NHD1286::Plot (int X, int Y)  
    {
    int row;
    int bit;
    
    Set_Col (X);
    row = Y / 8;     // Calculate byte row
    bit = Y % 8;     // Calculate bit
    Set_Row (row);
    Write (1 << bit);
    }
     
   //===========================================================
   void NHD1286::DispCommand (int Command, int Panel)
      {

    if (Panel == 1) 
      //void NHD1286::ComRight (int X)
      {
      PORTA = Command & 0xff;  // Set command
      PORTE |= P36;     // Chip select
      PORTB &= ~P30;    // Set instruction
      PORTB |= P34;     // E high
      _delay_us (10);
      PORTB &= ~P34;     // E Low
       _delay_us (10);
      PORTE &= ~P36;
      }
   
      else
      //void NHD1286::ComLeft (int X)
      {
      PORTA = Command & 0xff;
      PORTE |= P31;
      PORTB &= ~P30;
      PORTB |= P34;
       _delay_us (10);
      PORTB &= ~P34;
       _delay_us (10);
      PORTE &= ~P31;
      }
      }

   void NHD1286::DispData (char Data)
     {
     if (Active_Panel == 1)
      //void NHD1286::WriteRight (int X)
      {
      PORTA = Data & 0xff;
      PORTE |= P36;
      PORTB |= P30;
      PORTB |= P34;
       _delay_us (10);
      PORTB &= ~P34;
       _delay_us (10);
      PORTE &= ~P36;
      }
      else
      //void NHD1286::WriteLeft (int X)
      {
      PORTA = Data & 0xff;
      PORTE |= P31;
      PORTB |= P30;
      PORTB |= P34;
       _delay_us (10);
      PORTB &= ~P34;
       _delay_us (10);
      PORTE &= ~P31;
      }  
     }

  int NHD1286::DispDots (int Line, int col, char dots)
      {
      Set_Col (col);
      Set_Line (Line);
      DispData (dots);
     // col = Incr_Col ( );
      return col;        // Fix ??
      }
   
  int NHD1286::DispChar (char ch)
     {
     Font5x7 font;
     int dotcol;
     char dots;

    if (Col > 120)
       {
	    Set_Col (0);
	    Incr_Row ( );
        }

     switch (ch)
        {
        case '\r':
           Set_Col (0);
           break;
        case '\n': 
           Set_Row (Row + 1);
           break;
        default:
			for (dotcol = 0; dotcol < 7; dotcol++)
				{
				dots = font.FontDots (ch, dotcol);
				Col = DispDots (Row, Col, dots);			// Bumps col by 1
				Col = Incr_Col ( );
				}
           break;
        }
     return Col;
     }

  int NHD1286::DispString (char *ch)
     {
     //Font5x7 font;
     //int     dotcol;
     //char    dots;
            
        while (*ch != '\0')
           {
           //if (Col > 120)
              //{
              //Set_Col (0);
              //Incr_Row ( );
              //}
              
           DispChar (*ch);
           ch += 1;
           //for (dotcol = 0; dotcol < 7; dotcol++)
          //    {
           //   dots = font.FontDots (*ch, dotcol);
          //    Col = DispDots (Row, Col, dots);
           //   }
          // ch +=1;
           }
        return Col;
     }

   

   void NHD1286::DisppH (int row, int col, double pH)  // Display pH
      {
      //int U;
      //int H;
      
      
      }