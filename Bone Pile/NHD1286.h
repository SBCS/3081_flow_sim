/*
 * NHD1286.h
 *
 * Created: 6/15/2018 13:47:35
 *  Author: Fredc
 */ 

  #ifndef NHD1286_H_
  #define NHD1286_H_

  #include <avr/pgmspace.h> 
    


 class NHD1286
    {
     public:
       static void Init       (void);
       static void Clear      (void);						// Clear Display
       static void Display_ON (bool N);
       static void Set_Row    (int X);
       static void Set_Col    (int Y);
       static int  Get_Row    (void);
       static int  Get_Col    (void);
       static void Plot       (int X, int Y); 
       static int  DispChar   (char ch);     // Display Character
       static int  DispString (char *ch);		// Display Character
       static void Set_Line   (int L);
       static void DisppH     (int row, int col, double pH);  // Display pH

    protected:

    private:
	   static int  Active_Panel;
	   static int  Row;
	   static int  Col;
      static void Display_RST (bool R);					// Reset Display
      static void DispCommand (int Command, int Panel);	// Issue command to display
      static int  DispDots    (int Line, int col, char dots);  // Display Dots
      static int  Incr_Col    (void);
      static int  Incr_Row    (void);
      static void DispData    (char Data);						// Display data on panel
      static int  Read        (void);
      static int  Status      (void);
      static void Write       (int W);
       

   };

#endif /* NHD1286_H_ */