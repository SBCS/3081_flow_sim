/*
 * ANSI.h
 *
 * Created: 7/17/2018 12:04:59
 *  Author: Fredc
 */ 


#ifndef ANSI_H_
#define ANSI_H_

#define ESC 0x1b

   class ANSI
      {
      public:
         void Init ( );
         void Clear ( );
         void Put (char ch);
         void Put (char *ch);
         int  State ( );

      protected:

      private:
         int Ansi_state;
         int next_ANSI_state;
         int accum1;
         int accum2;
      };



#endif /* ANSI_H_ */
//Esc[Line;ColumnH                   Moves the cursor to the specified position (coordinates).
//Esc[Line;Columnf 	                Moves the cursor to the specified position (coordinates).
//                                   If you do not specify a position, the cursor moves (line 0, column 0).

//Esc[ValueA 	         Cursor Up specified number of lines
//Esc[ValueB 	         Cursor Down  by the specified number of lines
//Esc[ValueC 	         Cursor Forward by the specified number of columns If the cursor is already in the rightmost column, ignores this sequence.
//Esc[ValueD 	         Cursor Backward specified number of columns If the cursor is already in the leftmost column ignores this sequence.
//Esc[s 	               Save Cursor Position:
//Esc[u               	Restore Cursor Position:
//Esc[2J 	            Erase Display moves the cursor to the home position (line 0, column 0).
//Esc[K 	                Erase Line:
