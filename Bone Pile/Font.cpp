/*
 * Font.cpp
 *
 * Created: 6/19/2018 15:03:58
 *  Author: Fredc
 */ 

 #include "Font.h"
 //ASCII_5x7
#define BASE 32

   char Font5x7::FontDots (char ch, int dotcol)
      {
 

            /*
                0b00000000,     // character 0x00 column 1 ........
                0b00000000,     // character 0x00 column 2 ........
                0b00000000,     // character 0x00 column 3 ........
                0b00000000,     // character 0x00 column 4 ........
                0b00000000,     // character 0x00 column 5 ........
          
                0b00000000,     // character 0x01 column 1 ........
                0b00000000,     // character 0x01 column 2 ........
                0b00000000,     // character 0x01 column 3 ........
                0b00000000,     // character 0x01 column 4 ........
                0b00000000,     // character 0x01 column 5 ........
          
                0b00000000,     // character 0x02 column 1 ........
                0b00000000,     // character 0x02 column 2 ........
                0b00000000,     // character 0x02 column 3 ........
                0b00000000,     // character 0x02 column 4 ........
                0b00000000,     // character 0x02 column 5 ........
          
                0b00000000,     // character 0x03 column 1 ........
                0b00000000,     // character 0x03 column 2 ........
                0b00000000,     // character 0x03 column 3 ........
                0b00000000,     // character 0x03 column 4 ........
                0b00000000,     // character 0x03 column 5 ........
          
                0b00000000,     // character 0x04 column 1 ........
                0b00000000,     // character 0x04 column 2 ........
                0b00000000,     // character 0x04 column 3 ........
                0b00000000,     // character 0x04 column 4 ........
                0b00000000,     // character 0x04 column 5 ........
          
                0b00000000,     // character 0x05 column 1 ........
                0b00000000,     // character 0x05 column 2 ........
                0b00000000,     // character 0x05 column 3 ........
                0b00000000,     // character 0x05 column 4 ........
                0b00000000,     // character 0x05 column 5 ........
          
                0b00000000,     // character 0x06 column 1 ........
                0b00000000,     // character 0x06 column 2 ........
                0b00000000,     // character 0x06 column 3 ........
                0b00000000,     // character 0x06 column 4 ........
                0b00000000,     // character 0x06 column 5 ........
          
                0b00000000,     // character 0x07 column 1 ........
                0b00000000,     // character 0x07 column 2 ........
                0b00000000,     // character 0x07 column 3 ........
                0b00000000,     // character 0x07 column 4 ........
                0b00000000,     // character 0x07 column 5 ........
          
                0b00000000,     // character 0x08 column 1 ........
                0b00000000,     // character 0x08 column 2 ........
                0b00000000,     // character 0x08 column 3 ........
                0b00000000,     // character 0x08 column 4 ........
                0b00000000,     // character 0x08 column 5 ........
          
                0b00000000,     // character 0x09 column 1 ........
                0b00000000,     // character 0x09 column 2 ........
                0b00000000,     // character 0x09 column 3 ........
                0b00000000,     // character 0x09 column 4 ........
                0b00000000,     // character 0x09 column 5 ........
          
                0b00000000,     // character 0x0A column 1 ........
                0b00000000,     // character 0x0A column 2 ........
                0b00000000,     // character 0x0A column 3 ........
                0b00000000,     // character 0x0A column 4 ........
                0b00000000,     // character 0x0A column 5 ........
          
                0b00000000,     // character 0x0B column 1 ........
                0b00000000,     // character 0x0B column 2 ........
                0b00000000,     // character 0x0B column 3 ........
                0b00000000,     // character 0x0B column 4 ........
                0b00000000,     // character 0x0B column 5 ........
          
                0b00000000,     // character 0x0C column 1 ........
                0b00000000,     // character 0x0C column 2 ........
                0b00000000,     // character 0x0C column 3 ........
                0b00000000,     // character 0x0C column 4 ........
                0b00000000,     // character 0x0C column 5 ........
          
                0b00000000,     // character 0x0D column 1 ........
                0b00000000,     // character 0x0D column 2 ........
                0b00000000,     // character 0x0D column 3 ........
                0b00000000,     // character 0x0D column 4 ........
                0b00000000,     // character 0x0D column 5 ........
          
                0b00000000,     // character 0x0E column 1 ........
                0b00000000,     // character 0x0E column 2 ........
                0b00000000,     // character 0x0E column 3 ........
                0b00000000,     // character 0x0E column 4 ........
                0b00000000,     // character 0x0E column 5 ........
          
                0b00000000,     // character 0x0F column 1 ........
                0b00000000,     // character 0x0F column 2 ........
                0b00000000,     // character 0x0F column 3 ........
                0b00000000,     // character 0x0F column 4 ........
                0b00000000,     // character 0x0F column 5 ........

                0b01111111,     // character 0x10 column 1 .*******
                0b00000000,     // character 0x10 column 2 ........
                0b00000000,     // character 0x10 column 3 ........
                0b00000000,     // character 0x10 column 4 ........
                0b00000000,     // character 0x10 column 5 ........
          
                0b01111111,     // character 0x11 column 1 .*******
                0b01111111,     // character 0x11 column 2 .*******
                0b00000000,     // character 0x11 column 3 ........
                0b00000000,     // character 0x11 column 4 ........
                0b00000000,     // character 0x11 column 5 ........
          
                0b01111111,     // character 0x12 column 1 .*******
                0b01111111,     // character 0x12 column 2 .*******
                0b01111111,     // character 0x12 column 3 .*******
                0b00000000,     // character 0x12 column 4 ........
                0b00000000,     // character 0x12 column 5 ........
          
                0b01111111,     // character 0x13 column 1 .*******
                0b01111111,     // character 0x13 column 2 .*******
                0b01111111,     // character 0x13 column 3 .*******
                0b01111111,     // character 0x13 column 4 .*******
                0b00000000,     // character 0x13 column 5 ........
          
                0b01111111,     // character 0x14 column 1 .*******
                0b01111111,     // character 0x14 column 2 .*******
                0b01111111,     // character 0x14 column 3 .*******
                0b01111111,     // character 0x14 column 4 .*******
                0b01111111,     // character 0x14 column 5 .*******
          
                0b00000000,     // character 0x15 column 1 ........
                0b01111111,     // character 0x15 column 2 .*******
                0b01111111,     // character 0x15 column 3 .*******
                0b01111111,     // character 0x15 column 4 .*******
                0b01111111,     // character 0x15 column 5 .*******
          
                0b00000000,     // character 0x16 column 1 ........
                0b00000000,     // character 0x16 column 2 ........
                0b01111111,     // character 0x16 column 3 .*******
                0b01111111,     // character 0x16 column 4 .*******
                0b01111111,     // character 0x16 column 5 .*******
          
                0b00000000,     // character 0x17 column 1 ........
                0b00000000,     // character 0x17 column 2 ........
                0b00000000,     // character 0x17 column 3 ........
                0b01111111,     // character 0x17 column 4 .*******
                0b01111111,     // character 0x17 column 5 .*******
          
                0b00000000,     // character 0x18 column 1 ........
                0b00000000,     // character 0x18 column 2 ........
                0b00000000,     // character 0x18 column 3 ........
                0b00000000,     // character 0x18 column 4 ........
                0b01111111,     // character 0x18 column 5 .*******
          
                0b01100000,     // character 0x19 column 1 .**.....
                0b01110000,     // character 0x19 column 2 .***....
                0b00111111,     // character 0x19 column 3 ..******
                0b00000010,     // character 0x19 column 4 ......*.
                0b00111100,     // character 0x19 column 5 ..****..
          
                0b00000011,     // character 0x1A column 1 ......**
                0b00000011,     // character 0x1A column 2 ......**
                0b00110000,     // character 0x1A column 3 ..**....
                0b01001000,     // character 0x1A column 4 .*..*...
                0b01001000,     // character 0x1A column 5 .*..*...
          
                0b00000011,     // character 0x1B column 1 ......**
                0b01111011,     // character 0x1B column 2 .****.**
                0b00101000,     // character 0x1B column 3 ..*.*...
                0b00101000,     // character 0x1B column 4 ..*.*...
                0b00001000,     // character 0x1B column 5 ....*...
          
                0b00000100,     // character 0x1C column 1 .....*..
                0b00001100,     // character 0x1C column 2 ....**..
                0b00011100,     // character 0x1C column 3 ...***..
                0b00001100,     // character 0x1C column 4 ....**..
                0b00000100,     // character 0x1C column 5 .....*..
          
                0b00000000,     // character 0x1D column 1 ........
                0b00111110,     // character 0x1D column 2 ..*****.
                0b00011100,     // character 0x1D column 3 ...***..
                0b00001000,     // character 0x1D column 4 ....*...
                0b00000000,     // character 0x1D column 5 ........
          
                0b00000000,     // character 0x1E column 1 ........
                0b00001000,     // character 0x1E column 2 ....*...
                0b00011100,     // character 0x1E column 3 ...***..
                0b00111110,     // character 0x1E column 4 ..*****.
                0b00000000,     // character 0x1E column 5 ........
          
                0b00010000,     // character 0x1F column 1 ...*....
                0b00011000,     // character 0x1F column 2 ...**...
                0b00011100,     // character 0x1F column 3 ...***..
                0b00011000,     // character 0x1F column 4 ...**...
                0b00010000,     // character 0x1F column 5 ...*....
                */
                //=====================================================================

          switch (ch)
             {
             case ' ':
                switch (dotcol)
                   {
                   case 0: return 0; break;
                   case 1: return 0; break;
                   case 2: return 0; break;
                   case 3: return 0; break;
                   case 4: return 0; break;
                   }
                break;

                ////0b00000000,     // character 0x20 column 1 ........ Space
                ////0b00000000,     // character 0x20 column 2 ........
                ////0b00000000,     // character 0x20 column 3 ........
                ////0b00000000,     // character 0x20 column 4 ........
                ////0b00000000,     // character 0x20 column 5 ........

          ////
                ////0b00000000,     // character 0x21 column 1 ........
                ////0b00000000,     // character 0x21 column 2 ........
                ////0b01011111,     // character 0x21 column 3 .*.*****
                ////0b00000000,     // character 0x21 column 4 ........
                ////0b00000000,     // character 0x21 column 5 ........
          ////
                ////0b00000000,     // character 0x22 column 1 ........
                ////0b00000111,     // character 0x22 column 2 .....***
                ////0b00000000,     // character 0x22 column 3 ........
                ////0b00000111,     // character 0x22 column 4 .....***
                ////0b00000000,     // character 0x22 column 5 ........
          ////
                ////0b00010100,     // character 0x23 column 1 ...*.*..
                ////0b01111111,     // character 0x23 column 2 .*******
                ////0b00010100,     // character 0x23 column 3 ...*.*..
                ////0b01111111,     // character 0x23 column 4 .*******
                ////0b00010100,     // character 0x23 column 5 ...*.*..
          ////
                ////0b00100100,     // character 0x24 column 1 ..*..*..
                ////0b00101010,     // character 0x24 column 2 ..*.*.*.
                ////0b01111111,     // character 0x24 column 3 .*******
                ////0b00101010,     // character 0x24 column 4 ..*.*.*.
                ////0b00010010,     // character 0x24 column 5 ...*..*.
          ////
                ////0b00100011,     // character 0x25 column 1 ..*...**
                ////0b00010011,     // character 0x25 column 2 ...*..**
                ////0b00001000,     // character 0x25 column 3 ....*...
                ////0b01100100,     // character 0x25 column 4 .**..*..
                ////0b01100010,     // character 0x25 column 5 .**...*.
          ////
                ////0b00110110,     // character 0x26 column 1 ..**.**.
                ////0b01001001,     // character 0x26 column 2 .*..*..*
                ////0b01010101,     // character 0x26 column 3 .*.*.*.*
                ////0b00100010,     // character 0x26 column 4 ..*...*.
                ////0b01010000,     // character 0x26 column 5 .*.*....
          ////
                ////0b00000000,     // character 0x27 column 1 ........
                ////0b00000101,     // character 0x27 column 2 .....*.*
                ////0b00000011,     // character 0x27 column 3 ......**
                ////0b00000000,     // character 0x27 column 4 ........
                ////0b00000000,     // character 0x27 column 5 ........
          ////
                ////0b00000000,     // character 0x28 column 1 ........
                ////0b00011100,     // character 0x28 column 2 ...***..
                ////0b00100010,     // character 0x28 column 3 ..*...*.
                ////0b01000001,     // character 0x28 column 4 .*.....*
                ////0b00000000,     // character 0x28 column 5 ........
          ////
                ////0b00000000,     // character 0x29 column 1 ........
                ////0b01000001,     // character 0x29 column 2 .*.....*
                ////0b00100010,     // character 0x29 column 3 ..*...*.
                ////0b00011100,     // character 0x29 column 4 ...***..
                ////0b00000000,     // character 0x29 column 5 ........
          ////
          case '*':
                switch (dotcol)
                {
                   case 0: return 0b00001000; break;
                   case 1: return 0b00101010; break;
                   case 2: return 0b00011100; break;
                   case 3: return 0b00101010; break;
                   case 4: return 0b00001000; break;
                }
                break;

        case '+':
             switch (dotcol)
                    {
                       case 0: return 0b00001000; break;
                       case 1: return 0b00001000; break;
                       case 2: return 0b00111110; break;
                       case 3: return 0b00001000; break;
                       case 4: return 0b00001000; break;
                    }
                    break;

              case ',':
                    switch (dotcol)
                    {
                       case 0: return 0b00000000; break;
                       case 1: return 0b01010000; break;
                       case 2: return 0b00110000; break;
                       case 3: return 0b00000000; break;
                       case 4: return 0b00000000; break;
                    }
                    break;

            case '-':
                  switch (dotcol)
                  {
                     case 0: return 0b00001000; break;
                     case 1: return 0b00001000; break;
                     case 2: return 0b00001000; break;
                     case 3: return 0b00001000; break;
                     case 4: return 0b00001000; break;
                  }
                  break;

          case '.':
             switch (dotcol)
                {
                   case 0: return 0b00000000; break;
                   case 1: return 0b01100000; break;
                   case 2: return 0b01100000; break;
                   case 3: return 0b00000000; break;
                   case 4: return 0b00000000; break;
                }
                break;
                
                ////0b00100000,     // character 0x2F column 1 ..*.....
                ////0b00010000,     // character 0x2F column 2 ...*....
                ////0b00001000,     // character 0x2F column 3 ....*...
                ////0b00000100,     // character 0x2F column 4 .....*..
                ////0b00000010,     // character 0x2F column 5 ......*.
          ////
           case '0':
              switch (dotcol)
                 {
                  case 0: return 0b00111110; break;
                  case 1: return 0b01010001; break;
                  case 2: return 0b01001001; break;
                  case 3: return 0b01000101; break;
                  case 4: return 0b00111110; break;
                 }
                 break;

           case '1':
              switch (dotcol)
                {
                case 0: return 0b00000000; break;
                case 1: return 0b01000010; break;
                case 2: return 0b01111111; break;
                case 3: return 0b01000000; break;
                case 4: return 0b00000000; break;
                }
              break;

          case '2':
              switch (dotcol)
                {
                case 0: return 0b01000010; break;
                case 1: return 0b01100001; break;
                case 2: return 0b01010001; break;
                case 3: return 0b01001001; break;
                case 4: return 0b01000110; break;
                }
             break;

         case '3':
             switch (dotcol)
             {
                case 0: return 0b00100001; break;
                case 1: return 0b01000001; break;
                case 2: return 0b01000101; break;
                case 3: return 0b01001011; break;
                case 4: return 0b00110001; break;
             }
             break;

        case '4':
			switch (dotcol)
			{
			   case 0: return 0b00011000; break;
			   case 1: return 0b00010100; break;
			   case 2: return 0b00010010; break;
			   case 3: return 0b01111111; break;
			   case 4: return 0b00010000; break;
			}
			break;

        case '5':
        switch (dotcol)
        {
           case 0: return 0b00100111; break;
           case 1: return 0b01000101; break;
           case 2: return 0b01000101; break;
           case 3: return 0b01000101; break;
           case 4: return 0b00111001; break;
        }
        break;

        case '6':
           switch (dotcol)
           {
           case 0: return 0b00111100; break;
           case 1: return 0b01001010; break;
           case 2: return 0b01001001; break;
           case 3: return 0b01001001; break;
           case 4: return 0b00110000; break;
           }
           break;

        case '7':
           switch (dotcol)
              {
              case 0: return 0b00000001; break;
              case 1: return 0b01110001; break;
              case 2: return 0b00001001; break;
              case 3: return 0b00000101; break;
              case 4: return 0b00000011; break;
              }
           break;

        case '8':
             switch (dotcol)
                   {
                      case 0: return 0b00110110; break;
                      case 1: return 0b01001001; break;
                      case 2: return 0b01001001; break;
                      case 3: return 0b01001001; break;
                      case 4: return 0b00110110; break;
                   }
                   break;

        case '9':
                switch (dotcol)
                   {
                   case 0: return 0b00000110; break;
                   case 1: return 0b01001001; break;
                   case 2: return 0b01001001; break;
                   case 3: return 0b00101001; break;
                   case 4: return 0b00011110; break;
                   }
                break;
     
      
          ////
                ////0b00000000,     // character 0x3A column 1 ........
                ////0b00110110,     // character 0x3A column 2 ..**.**.
                ////0b00110110,     // character 0x3A column 3 ..**.**.
                ////0b00000000,     // character 0x3A column 4 ........
                ////0b00000000,     // character 0x3A column 5 ........
          ////
                ////0b00000000,     // character 0x3B column 1 ........
                ////0b01010110,     // character 0x3B column 2 .*.*.**.
                ////0b00110110,     // character 0x3B column 3 ..**.**.
                ////0b00000000,     // character 0x3B column 4 ........
                ////0b00000000,     // character 0x3B column 5 ........
          ////
                ////0b00000000,     // character 0x3C column 1 ........
                ////0b00001000,     // character 0x3C column 2 ....*...
                ////0b00010100,     // character 0x3C column 3 ...*.*..
                ////0b00100010,     // character 0x3C column 4 ..*...*.
                ////0b01000001,     // character 0x3C column 5 .*.....*
          ////
                ////0b00010100,     // character 0x3D column 1 ...*.*..
                ////0b00010100,     // character 0x3D column 2 ...*.*..
                ////0b00010100,     // character 0x3D column 3 ...*.*..
                ////0b00010100,     // character 0x3D column 4 ...*.*..
                ////0b00010100,     // character 0x3D column 5 ...*.*..
          ////
                ////0b01000001,     // character 0x3E column 1 .*.....*
                ////0b00100010,     // character 0x3E column 2 ..*...*.
                ////0b00010100,     // character 0x3E column 3 ...*.*..
                ////0b00001000,     // character 0x3E column 4 ....*...
                ////0b00000000,     // character 0x3E column 5 ........
          ////
                ////0b00000010,     // character 0x3F column 1 ......*.
                ////0b00000001,     // character 0x3F column 2 .......*
                ////0b01010001,     // character 0x3F column 3 .*.*...*
                ////0b00001001,     // character 0x3F column 4 ....*..*
                ////0b00000110,     // character 0x3F column 5 .....**.
          ////
                ////0b00110010,     // character 0x40 column 1 ..**..*.
                ////0b01001001,     // character 0x40 column 2 .*..*..*
                ////0b01111001,     // character 0x40 column 3 .****..*
                ////0b01000001,     // character 0x40 column 4 .*.....*
                ////0b00111110,     // character 0x40 column 5 ..*****.
          ////
          case 'A':
             switch (dotcol)
             {
             case 0: return 0b01111110; break;
             case 1: return 0b00010001; break;
             case 2: return 0b00010001; break;
             case 3: return 0b00010001; break;
             case 4: return 0b01111110; break;
             }
          break;

        case 'B':
                    switch (dotcol)
                    {
                       case 0: return 0b01111111; break;
                       case 1: return 0b01001001; break;
                       case 2: return 0b01001001; break;
                       case 3: return 0b01001001; break;
                       case 4: return 0b00110110; break;
                    }
                    break;

        case 'C':
                switch (dotcol)
                {
                   case 0: return 0b00111110; break;
                   case 1: return 0b01000001; break;
                   case 2: return 0b01000001; break;
                   case 3: return 0b01000001; break;
                   case 4: return 0b00100010; break;
                }
                break;

       case 'D':
             switch (dotcol)
             {
                case 0: return 0b01111111; break;
                case 1: return 0b01000001; break;
                case 2: return 0b01000001; break;
                case 3: return 0b00100010; break;
                case 4: return 0b00011100; break;
             }
             break;

             
     case 'E':
             switch (dotcol)
             {
                case 0: return 0b01111111; break;
                case 1: return 0b01001001; break;
                case 2: return 0b01001001; break;
                case 3: return 0b01001001; break;
                case 4: return 0b01000001; break;
             }
             break;

                          
     case 'F':
          switch (dotcol)
              {
                 case 0: return 0b01111111; break;
                 case 1: return 0b00001001; break;
                 case 2: return 0b00001001; break;
                 case 3: return 0b00000001; break;
                 case 4: return 0b00000001; break;
              }
             break;

                                       
     case 'G':
          switch (dotcol)
               {
                  case 0: return 0b00111110; break;
                  case 1: return 0b01000001; break;
                  case 2: return 0b01000001; break;
                  case 3: return 0b01010001; break;
                  case 4: return 0b00110010; break;
               }
               break;
                                       
      case 'H':
        switch (dotcol)
             {
                case 0: return 0b01111111; break;
                case 1: return 0b00001000; break;
                case 2: return 0b00001000; break;
                case 3: return 0b00001000; break;
                case 4: return 0b01111111; break;
             }
             break;
 
     case 'I':
       switch (dotcol)
       {
          case 0: return 0b00000000; break;
          case 1: return 0b01000001; break;
          case 2: return 0b01111111; break;
          case 3: return 0b01000001; break;
          case 4: return 0b00000000; break;
       }
       break;
   
     case 'J':
        switch (dotcol)
        {
           case 0: return 0b00100000; break;
           case 1: return 0b01000000; break;
           case 2: return 0b01000001; break;
           case 3: return 0b00111111; break;
           case 4: return 0b00000001; break;
        }
        break;

       case 'K':
             switch (dotcol)
             {
                case 0: return 0b01111111; break;
                case 1: return 0b00001000; break;
                case 2: return 0b00010100; break;
                case 3: return 0b00100010; break;
                case 4: return 0b01000001; break;
             }
             break;

       case 'L':
             switch (dotcol)
             {
                case 0: return 0b01111111; break;
                case 1: return 0b01000000; break;
                case 2: return 0b01000000; break;
                case 3: return 0b01000000; break;
                case 4: return 0b01000000; break;
             }
             break;

        case 'M':
              switch (dotcol)
              {
                 case 0: return 0b01111111; break;
                 case 1: return 0b00000010; break;
                 case 2: return 0b00000100; break;
                 case 3: return 0b00000010; break;
                 case 4: return 0b01111111; break;
              }
              break;

        case 'N':
              switch (dotcol)
              {
                 case 0: return 0b01111111; break;
                 case 1: return 0b00000100; break;
                 case 2: return 0b00001000; break;
                 case 3: return 0b00010000; break;
                 case 4: return 0b01111111; break;
              }
              break;

       case 'O':
              switch (dotcol)
              {
                 case 0: return 0b00111110; break;
                 case 1: return 0b01000001; break;
                 case 2: return 0b01000001; break;
                 case 3: return 0b01000001; break;
                 case 4: return 0b00111110; break;
              }
              break;
         
       case 'P':
                switch (dotcol)
                {
                   case 0: return 0b01111111; break;
                   case 1: return 0b00001001; break;
                   case 2: return 0b00001001; break;
                   case 3: return 0b00001001; break;
                   case 4: return 0b00000110; break;
                }
                break;

       case 'Q':
          switch (dotcol)
             {
                case 0: return 0b00111110; break;
                case 1: return 0b01000001; break;
                case 2: return 0b01010001; break;
                case 3: return 0b00100001; break;
                case 4: return 0b01011110; break;
             }
             break;

     case 'R':
                switch (dotcol)
                {
                   case 0: return 0b01111111; break;
                   case 1: return 0b00001001; break;
                   case 2: return 0b00011001; break;
                   case 3: return 0b00101001; break;
                   case 4: return 0b01000110; break;
                }
                break;
    case 'S':
                switch (dotcol)
                {
                   case 0: return 0b01000110; break;
                   case 1: return 0b01001001; break;
                   case 2: return 0b01001001; break;
                   case 3: return 0b01001001; break;
                   case 4: return 0b00110001; break;
                }
                break;

    case 'T':
             switch (dotcol)
             {
                case 0: return 0b00000001;  break;
                case 1: return 0b00000001;  break;
                case 2: return 0b01111111;  break;
                case 3: return 0b00000001;  break;
                case 4: return 0b00000001;  break;
             }
             break;

   case 'U':
         switch (dotcol)
         {
            case 0: return 0b00111111;  break;
            case 1: return 0b01000000;  break;
            case 2: return 0b01000000;  break;
            case 3: return 0b01000000;  break;
            case 4: return 0b00111111;  break;
         }
         break;
        
     case 'V':
           switch (dotcol)
           {
              case 0: return 0b00011111;  break;
              case 1: return 0b00100000;  break;
              case 2: return 0b01000000;  break;
              case 3: return 0b00100000;  break;
              case 4: return 0b00011111;  break;
           }
           break;

        case 'W':
              switch (dotcol)
              {
                 case 0: return 0b01111111; break;
                 case 1: return 0b00100000; break;
                 case 2: return 0b00011000; break;
                 case 3: return 0b00100000; break;
                 case 4: return 0b01111111; break;
              }
              break;

        case 'X':
              switch (dotcol)
              {
                 case 0: return 0b1100011; break;
                 case 1: return 0b0010100; break;
                 case 2: return 0b0001000; break;
                 case 3: return 0b0010100; break;
                 case 4: return 0b1100011; break;
              }
              break;


              case 'Y':
                      switch (dotcol)
                      {
                         case 0: return 0b00000011; break;
                         case 1: return 0b00000100; break;
                         case 2: return 0b01111000; break;
                         case 3: return 0b00000100; break;
                         case 4: return 0b00000011; break;
                      }
                      break;

              case 'Z':
                   switch (dotcol)
                      {
                      case 0: return 0b01100001; break;
                      case 1: return 0b01010001; break;
                      case 2: return 0b01001001; break;
                      case 3: return 0b01000101; break;
                      case 4: return 0b01000011; break;
                      }
                   break;

          ////

          ////
                ////0b00000000,     // character 0x5B column 1 ........
                ////0b00000000,     // character 0x5B column 2 ........
                ////0b01111111,     // character 0x5B column 3 .*******
                ////0b01000001,     // character 0x5B column 4 .*.....*
                ////0b01000001,     // character 0x5B column 5 .*.....*
          ////
                ////0b00000010,     // character 0x5C column 1 ......*.
                ////0b00000100,     // character 0x5C column 2 .....*..
                ////0b00001000,     // character 0x5C column 3 ....*...
                ////0b00010000,     // character 0x5C column 4 ...*....
                ////0b00100000,     // character 0x5C column 5 ..*.....
          ////
                ////0b01000001,     // character 0x5D column 1 .*.....*
                ////0b01000001,     // character 0x5D column 2 .*.....*
                ////0b01111111,     // character 0x5D column 3 .*******
                ////0b00000000,     // character 0x5D column 4 ........
                ////0b00000000,     // character 0x5D column 5 ........
          ////
                ////0b00000100,     // character 0x5E column 1 .....*..
                ////0b00000010,     // character 0x5E column 2 ......*.
                ////0b00000001,     // character 0x5E column 3 .......*
                ////0b00000010,     // character 0x5E column 4 ......*.
                ////0b00000100,     // character 0x5E column 5 .....*..
          ////
		      case '_':
		          switch (dotcol)
		             {
			          case 0: return 0b01000000; break;
			          case 1: return 0b01000000; break;
			          case 2: return 0b01000000; break;
			          case 3: return 0b01000000; break;
			          case 4: return 0b01000000; break;
		             }
		          break;
		  
		  
                ////0b00000000,     // character 0x60 column 1 ........
                ////0b00000001,     // character 0x60 column 2 .......*
                ////0b00000010,     // character 0x60 column 3 ......*.
                ////0b00000100,     // character 0x60 column 4 .....*..
                ////0b00000000,     // character 0x60 column 5 ........
          
          case 'a':
             switch (dotcol)
                {
                   case 0: return 0b00100000; break;
                   case 1: return 0b01010100; break;
                   case 2: return 0b01010100; break;
                   case 3: return 0b01010100; break;
                   case 4: return 0b01111000; break;
                }
                break;

          case 'b':
               switch (dotcol)
                    {
                       case 0: return 0b01111111; break;
                       case 1: return 0b01001000; break;
                       case 2: return 0b01000100; break;
                       case 3: return 0b01000100; break;
                       case 4: return 0b00111000; break;
                    }
                    break;
         case 'c':
                switch (dotcol)
                   {
                   case 0: return 0b00111000; break;
                   case 1: return 0b01000100; break;
                   case 2: return 0b01000100; break;
                   case 3: return 0b01000100; break;
                   case 4: return 0b00100000; break;
                   }
                break;

        case 'd':
                switch (dotcol)
                {
                   case 0: return 0b00111000; break;
                   case 1: return 0b01000100; break;
                   case 2: return 0b01000100; break;
                   case 3: return 0b01001000; break;
                   case 4: return 0b01111111; break;
                }
                break;

        case 'e':
			switch (dotcol)
			{
				case 0: return 0b00111000; break;
				case 1: return 0b01010100; break;
				case 2: return 0b01010100; break;
				case 3: return 0b01010100; break;
				case 4: return 0b00011000; break;
			}
			break;

           case 'f':
               switch (dotcol)
               {
	               case 0: return 0b00001000; break;
	               case 1: return 0b01111110; break;
	               case 2: return 0b00001001; break;
	               case 3: return 0b00000001; break;
	               case 4: return 0b00000010; break;
               }
               break;
  
           case 'g':
			   switch (dotcol)
			   {
				   case 0: return 0b00001000; break;
				   case 1: return 0b00010100; break;
				   case 2: return 0b01010100; break;
				   case 3: return 0b01010100; break;
				   case 4: return 0b00111100; break;
			   }
			   break;

           case 'h':
			   switch (dotcol)
			   {
				   case 0: return 0b01111111; break;
				   case 1: return 0b00001000; break;
				   case 2: return 0b00000100; break;
				   case 3: return 0b00000100; break;
				   case 4: return 0b01111000; break;
			   }
			   break;

           case 'i':
				   switch (dotcol)
				   {
					   case 0: return 0b00000000; break;
					   case 1: return 0b01000100; break;
					   case 2: return 0b01111101; break;
					   case 3: return 0b01000000; break;
					   case 4: return 0b00000000; break;
				   }
				   break;
      
           case 'j':
			   switch (dotcol)
			   {
				   case 0: return 0b00100000; break;
				   case 1: return 0b01000000; break;
				   case 2: return 0b01000100; break;
				   case 3: return 0b00111101; break;
				   case 4: return 0b00000000; break;
			   }
			   break;


               case 'k':
			   switch (dotcol)
			   {
				   case 0: return 0b00000000; break;
				   case 1: return 0b01111111; break;
				   case 2: return 0b00010000; break;
				   case 3: return 0b00101000; break;
				   case 4: return 0b01000100; break;
			   }
			   break;

           case 'l':
			   switch (dotcol)
			   {
				   case 0: return 0b00000000; break;
				   case 1: return 0b01000001; break;
				   case 2: return 0b01111111; break;
				   case 3: return 0b01000000; break;
				   case 4: return 0b00000000; break;
			   }
			   break;

           case 'm':
                  switch (dotcol)
                  {
                     case 0: return 0b01111100; break;
                     case 1: return 0b00000100; break;
                     case 2: return 0b00011000; break;
                     case 3: return 0b00000100; break;
                     case 4: return 0b01111000; break;
                  }
                  break;

           case 'n':
				   switch (dotcol)
				   {
					   case 0: return 0b01111100; break;
					   case 1: return 0b00001000; break;
					   case 2: return 0b00000100; break;
					   case 3: return 0b00000100; break;
					   case 4: return 0b01111000; break;
				   }
				   break;

           case 'o':
			   switch (dotcol)
			   {
				   case 0: return 0b00111000; break;
				   case 1: return 0b01000100; break;
				   case 2: return 0b01000100; break;
				   case 3: return 0b01000100; break;
				   case 4: return 0b00111000; break;
			   }
			   break;
         
            case 'p':
                    switch (dotcol)
                    {
                       case 0: return 0b01111100; break;
                       case 1: return 0b00010100; break;
                       case 2: return 0b00010100; break;
                       case 3: return 0b00010100; break;
                       case 4: return 0b00001000; break;
                    }
                    break;

		 case 'q':
				 switch (dotcol)
				 {
					 case 0: return 0b00001000; break;
					 case 1: return 0b00010100; break;
					 case 2: return 0b00010100; break;
					 case 3: return 0b00011000; break;
					 case 4: return 0b01111100; break;
				 }
				 break;

		 case 'r':
			 switch (dotcol)
				 {
					 case 0: return 0b01111100; break;
					 case 1: return 0b00001000; break;
					 case 2: return 0b00000100; break;
					 case 3: return 0b00000100; break;
					 case 4: return 0b00001000; break;
				 }
			 break;

		 case 's':
			switch (dotcol)
			{
				case 0: return 0b01001000; break;
				case 1: return 0b01010100; break;
				case 2: return 0b01010100; break;
				case 3: return 0b01010100; break;
				case 4: return 0b00100000; break;
			}
			break;
         
		 case 't':
			 switch (dotcol)
			 {
				 case 0: return 0b00000100; break;
				 case 1: return 0b00111111; break;
				 case 2: return 0b01000100; break;
				 case 3: return 0b01000000; break;
				 case 4: return 0b00100000; break;
			 }
			 break;

		 case 'u':
			 switch (dotcol)
			 {
				 case 0: return 0b00111100; break;
				 case 1: return 0b01000000; break;
				 case 2: return 0b01000000; break;
				 case 3: return 0b00100000; break;
				 case 4: return 0b01111100; break;
			 }
			 break;

		 case 'v':
			 switch (dotcol)
			 {
				 case 0: return 0b00011100; break;
				 case 1: return 0b00100000; break;
				 case 2: return 0b01000000; break;
				 case 3: return 0b00100000; break;
				 case 4: return 0b00011100; break;
			 }
			 break;

		 case 'w':
			 switch (dotcol)
			 {
				 case 0: return 0b00111100; break;
				 case 1: return 0b01000000; break;
				 case 2: return 0b00110000; break;
				 case 3: return 0b01000000; break;
				 case 4: return 0b00111100; break;
			 }
			 break;

		 case 'x':
			 switch (dotcol)
			 {
				 case 0: return 0b01000100; break;
				 case 1: return 0b00101000; break;
				 case 2: return 0b00010000; break;
				 case 3: return 0b00101000; break;
				 case 4: return 0b01000100; break;
			 }
			 break;
        
		 case 'y':
			 switch (dotcol)
			 {
				 case 0: return 0b00001100; break;
				 case 1: return 0b01010000; break;
				 case 2: return 0b01010000; break;
				 case 3: return 0b01010000; break;
				 case 4: return 0b00111100; break;
			 }
			 break;

		 case 'z':
			 switch (dotcol)
			 {
				 case 0: return 0b01000100; break;
				 case 1: return 0b01100100; break;
				 case 2: return 0b01010100; break;
				 case 3: return 0b01001100; break;
				 case 4: return 0b01000100; break;
			 }
			 break;




          ////===================================================================
                ////0b00000000,     // character 0x7B column 1 ........
                ////0b00001000,     // character 0x7B column 2 ....*...
                ////0b00110110,     // character 0x7B column 3 ..**.**.
                ////0b01000001,     // character 0x7B column 4 .*.....*
                ////0b00000000,     // character 0x7B column 5 ........
          ////
                ////0b00000000,     // character 0x7C column 1 ........
                ////0b00000000,     // character 0x7C column 2 ........
                ////0b01111111,     // character 0x7C column 3 .*******
                ////0b00000000,     // character 0x7C column 4 ........
                ////0b00000000,     // character 0x7C column 5 ........
          ////
                ////0b00000000,     // character 0x7D column 1 ........
                ////0b01000001,     // character 0x7D column 2 .*.....*
                ////0b00110110,     // character 0x7D column 3 ..**.**.
                ////0b00001000,     // character 0x7D column 4 ....*...
                ////0b00000000,     // character 0x7D column 5 ........
        
              case '~':          // 7E
                 switch (dotcol)
                 {
                    case 0: return 0b00001000; break;
                    case 1: return 0b00010000; break;
                    case 2: return 0b00001000; break;
                    case 3: return 0b00000100; break;
                    case 4: return 0b00001000; break;
                 }
                 break;
          ////
                ////0b00001000,     // character 0x7F column 1 ....*...
                ////0b00011100,     // character 0x7F column 2 ...***..
                ////0b00101010,     // character 0x7F column 3 ..*.*.*.
                ////0b00001000,     // character 0x7F column 4 ....*...
                ////0b00001000      // character 0x7F column 5 ....*...
                //==================================================================
                /*
                0b01110000,     // character 0x80 column 1 .***....
                0b00101001,     // character 0x80 column 2 ..*.*..*
                0b00100100,     // character 0x80 column 3 ..*..*..
                0b00101001,     // character 0x80 column 4 ..*.*..*
                0b01110000,     // character 0x80 column 5 .***....
          
                0b01110000,     // character 0x81 column 1 .***....
                0b00101000,     // character 0x81 column 2 ..*.*...
                0b00100101,     // character 0x81 column 3 ..*..*.*
                0b00101000,     // character 0x81 column 4 ..*.*...
                0b01110000,     // character 0x81 column 5 .***....
          
                0b01110000,     // character 0x82 column 1 .***....
                0b00101010,     // character 0x82 column 2 ..*.*.*.
                0b00100101,     // character 0x82 column 3 ..*..*.*
                0b00101010,     // character 0x82 column 4 ..*.*.*.
                0b01110000,     // character 0x82 column 5 .***....
          
                0b00100000,     // character 0x83 column 1 ..*.....
                0b01010100,     // character 0x83 column 2 .*.*.*..
                0b01010110,     // character 0x83 column 3 .*.*.**.
                0b01010101,     // character 0x83 column 4 .*.*.*.*
                0b01111000,     // character 0x83 column 5 .****...
          
                0b00100000,     // character 0x84 column 1 ..*.....
                0b01010100,     // character 0x84 column 2 .*.*.*..
                0b01010101,     // character 0x84 column 3 .*.*.*.*
                0b01010100,     // character 0x84 column 4 .*.*.*..
                0b01111000,     // character 0x84 column 5 .****...
          
                0b01111111,     // character 0x85 column 1 .*******
                0b01111111,     // character 0x85 column 2 .*******
                0b01001001,     // character 0x85 column 3 .*..*..*
                0b01001001,     // character 0x85 column 4 .*..*..*
                0b01001001,     // character 0x85 column 5 .*..*..*
          
                0b00111101,     // character 0x86 column 1 ..****.*
                0b01000010,     // character 0x86 column 2 .*....*.
                0b01000010,     // character 0x86 column 3 .*....*.
                0b01000010,     // character 0x86 column 4 .*....*.
                0b00111101,     // character 0x86 column 5 ..****.*
          
                0b00110000,     // character 0x87 column 1 ..**....
                0b01001010,     // character 0x87 column 2 .*..*.*.
                0b01001000,     // character 0x87 column 3 .*..*...
                0b01001010,     // character 0x87 column 4 .*..*.*.
                0b00110000,     // character 0x87 column 5 ..**....
          
                0b01011100,     // character 0x88 column 1 .*.***..
                0b00110010,     // character 0x88 column 2 ..**..*.
                0b00101010,     // character 0x88 column 3 ..*.*.*.
                0b00100110,     // character 0x88 column 4 ..*..**.
                0b00011101,     // character 0x88 column 5 ...***.*
          
                0b00011000,     // character 0x89 column 1 ...**...
                0b01100100,     // character 0x89 column 2 .**..*..
                0b00111100,     // character 0x89 column 3 ..****..
                0b00100110,     // character 0x89 column 4 ..*..**.
                0b00011000,     // character 0x89 column 5 ...**...
          
                0b00111101,     // character 0x8A column 1 ..****.*
                0b01000000,     // character 0x8A column 2 .*......
                0b01000000,     // character 0x8A column 3 .*......
                0b01000000,     // character 0x8A column 4 .*......
                0b00111101,     // character 0x8A column 5 ..****.*
          
                0b00111000,     // character 0x8B column 1 ..***...
                0b01000010,     // character 0x8B column 2 .*....*.
                0b01000000,     // character 0x8B column 3 .*......
                0b00100010,     // character 0x8B column 4 ..*...*.
                0b01111000,     // character 0x8B column 5 .****...
          
                0b00010101,     // character 0x8C column 1 ...*.*.*
                0b00010110,     // character 0x8C column 2 ...*.**.
                0b01111100,     // character 0x8C column 3 .*****..
                0b00010110,     // character 0x8C column 4 ...*.**.
                0b00010101,     // character 0x8C column 5 ...*.*.*
          
                0b01010100,     // character 0x8D column 1 .*.*.*..
                0b00110100,     // character 0x8D column 2 ..**.*..
                0b00011100,     // character 0x8D column 3 ...***..
                0b00010110,     // character 0x8D column 4 ...*.**.
                0b00010101,     // character 0x8D column 5 ...*.*.*
          
                0b00011000,     // character 0x8E column 1 ...**...
                0b00000100,     // character 0x8E column 2 .....*..
                0b00011000,     // character 0x8E column 3 ...**...
                0b00100000,     // character 0x8E column 4 ..*.....
                0b00011000,     // character 0x8E column 5 ...**...
          
                0b01001010,     // character 0x8F column 1 .*..*.*.
                0b01010101,     // character 0x8F column 2 .*.*.*.*
                0b01010101,     // character 0x8F column 3 .*.*.*.*
                0b01010101,     // character 0x8F column 4 .*.*.*.*
                0b00101001,     // character 0x8F column 5 ..*.*..*

                0b01111110,     // character 0x90 column 1 .******.
                0b00001001,     // character 0x90 column 2 ....*..*
                0b01111111,     // character 0x90 column 3 .*******
                0b00001001,     // character 0x90 column 4 ....*..*
                0b00001001,     // character 0x90 column 5 ....*..*
          
                0b00110100,     // character 0x91 column 1 ..**.*..
                0b01010100,     // character 0x91 column 2 .*.*.*..
                0b00111000,     // character 0x91 column 3 ..***...
                0b01010100,     // character 0x91 column 4 .*.*.*..
                0b01011000,     // character 0x91 column 5 .*.**...
          
                0b01001000,     // character 0x92 column 1 .*..*...
                0b01111110,     // character 0x92 column 2 .******.
                0b01001001,     // character 0x92 column 3 .*..*..*
                0b01001001,     // character 0x92 column 4 .*..*..*
                0b01000010,     // character 0x92 column 5 .*....*.
          
                0b01111111,     // character 0x93 column 1 .*******
                0b00000101,     // character 0x93 column 2 .....*.*
                0b00010101,     // character 0x93 column 3 ...*.*.*
                0b01111010,     // character 0x93 column 4 .****.*.
                0b01010000,     // character 0x93 column 5 .*.*....
          
                0b00011100,     // character 0x94 column 1 ...***..
                0b00111110,     // character 0x94 column 2 ..*****.
                0b00111110,     // character 0x94 column 3 ..*****.
                0b00111110,     // character 0x94 column 4 ..*****.
                0b00011100,     // character 0x94 column 5 ...***..
          
                0b00011100,     // character 0x95 column 1 ...***..
                0b00100010,     // character 0x95 column 2 ..*...*.
                0b00100010,     // character 0x95 column 3 ..*...*.
                0b00100010,     // character 0x95 column 4 ..*...*.
                0b00011100,     // character 0x95 column 5 ...***..
          
                0b00001000,     // character 0x96 column 1 ....*...
                0b00011100,     // character 0x96 column 2 ...***..
                0b00111110,     // character 0x96 column 3 ..*****.
                0b00011100,     // character 0x96 column 4 ...***..
                0b00001000,     // character 0x96 column 5 ....*...
          
                0b00001000,     // character 0x97 column 1 ....*...
                0b00010100,     // character 0x97 column 2 ...*.*..
                0b00100010,     // character 0x97 column 3 ..*...*.
                0b00010100,     // character 0x97 column 4 ...*.*..
                0b00001000,     // character 0x97 column 5 ....*...
          
                0b00000000,     // character 0x98 column 1 ........
                0b00000000,     // character 0x98 column 2 ........
                0b01110111,     // character 0x98 column 3 .***.***
                0b00000000,     // character 0x98 column 4 ........
                0b00000000,     // character 0x98 column 5 ........
          
                0b00001110,     // character 0x99 column 1 ....***.
                0b01010001,     // character 0x99 column 2 .*.*...*
                0b00110001,     // character 0x99 column 3 ..**...*
                0b00010001,     // character 0x99 column 4 ...*...*
                0b00010001,     // character 0x99 column 5 ...*...*
          
                0b01100000,     // character 0x9A column 1 .**.....
                0b01010000,     // character 0x9A column 2 .*.*....
                0b01001000,     // character 0x9A column 3 .*..*...
                0b01000100,     // character 0x9A column 4 .*...*..
                0b01111110,     // character 0x9A column 5 .******.
          
                0b01000000,     // character 0x9B column 1 .*......
                0b01000100,     // character 0x9B column 2 .*...*..
                0b01001010,     // character 0x9B column 3 .*..*.*.
                0b01010001,     // character 0x9B column 4 .*.*...*
                0b01000000,     // character 0x9B column 5 .*......
          
                0b01000000,     // character 0x9C column 1 .*......
                0b01010001,     // character 0x9C column 2 .*.*...*
                0b01001010,     // character 0x9C column 3 .*..*.*.
                0b01000100,     // character 0x9C column 4 .*...*..
                0b01000000,     // character 0x9C column 5 .*......
          
                0b00010000,     // character 0x9D column 1 ...*....
                0b00111000,     // character 0x9D column 2 ..***...
                0b01010100,     // character 0x9D column 3 .*.*.*..
                0b00010000,     // character 0x9D column 4 ...*....
                0b00011111,     // character 0x9D column 5 ...*****
          
                0b00000100,     // character 0x9E column 1 .....*..
                0b00000010,     // character 0x9E column 2 ......*.
                0b01111111,     // character 0x9E column 3 .*******
                0b00000010,     // character 0x9E column 4 ......*.
                0b00000100,     // character 0x9E column 5 .....*..
          
                0b00010000,     // character 0x9F column 1 ...*....
                0b00100000,     // character 0x9F column 2 ..*.....
                0b01111111,     // character 0x9F column 3 .*******
                0b00100000,     // character 0x9F column 4 ..*.....
                0b00010000,     // character 0x9F column 5 ...*....

                0b00000000,     // character 0xA0 column 1 ........
                0b00000000,     // character 0xA0 column 2 ........
                0b00000000,     // character 0xA0 column 3 ........
                0b00000000,     // character 0xA0 column 4 ........
                0b00000000,     // character 0xA0 column 5 ........
          
                0b01110000,     // character 0xA1 column 1 .***....
                0b01010000,     // character 0xA1 column 2 .*.*....
                0b01110000,     // character 0xA1 column 3 .***....
                0b00000000,     // character 0xA1 column 4 ........
                0b00000000,     // character 0xA1 column 5 ........
          
                0b00000000,     // character 0xA2 column 1 ........
                0b00000000,     // character 0xA2 column 2 ........
                0b00001111,     // character 0xA2 column 3 ....****
                0b00000001,     // character 0xA2 column 4 .......*
                0b00000001,     // character 0xA2 column 5 .......*
          
                0b01000000,     // character 0xA3 column 1 .*......
                0b01000000,     // character 0xA3 column 2 .*......
                0b01111000,     // character 0xA3 column 3 .****...
                0b00000000,     // character 0xA3 column 4 ........
                0b00000000,     // character 0xA3 column 5 ........
          
                0b00010000,     // character 0xA4 column 1 ...*....
                0b00100000,     // character 0xA4 column 2 ..*.....
                0b01000000,     // character 0xA4 column 3 .*......
                0b00000000,     // character 0xA4 column 4 ........
                0b00000000,     // character 0xA4 column 5 ........
          
                0b00000000,     // character 0xA5 column 1 ........
                0b00110000,     // character 0xA5 column 2 ..**....
                0b00110000,     // character 0xA5 column 3 ..**....
                0b00000000,     // character 0xA5 column 4 ........
                0b00000000,     // character 0xA5 column 5 ........
          
                0b00000101,     // character 0xA6 column 1 .....*.*
                0b00000101,     // character 0xA6 column 2 .....*.*
                0b01000101,     // character 0xA6 column 3 .*...*.*
                0b00100101,     // character 0xA6 column 4 ..*..*.*
                0b00011111,     // character 0xA6 column 5 ...*****
          
                0b00000000,     // character 0xA7 column 1 ........
                0b00000000,     // character 0xA7 column 2 ........
                0b00000000,     // character 0xA7 column 3 ........
                0b00000000,     // character 0xA7 column 4 ........
                0b00000000,     // character 0xA7 column 5 ........
          
                0b00000100,     // character 0xA8 column 1 .....*..
                0b01000100,     // character 0xA8 column 2 .*...*..
                0b00110100,     // character 0xA8 column 3 ..**.*..
                0b00010100,     // character 0xA8 column 4 ...*.*..
                0b00001100,     // character 0xA8 column 5 ....**..
          
                0b00100000,     // character 0xA9 column 1 ..*.....
                0b00010000,     // character 0xA9 column 2 ...*....
                0b01111000,     // character 0xA9 column 3 .****...
                0b00000100,     // character 0xA9 column 4 .....*..
                0b00000000,     // character 0xA9 column 5 ........
          
                0b00011000,     // character 0xAA column 1 ...**...
                0b00001000,     // character 0xAA column 2 ....*...
                0b01001100,     // character 0xAA column 3 .*..**..
                0b00111000,     // character 0xAA column 4 ..***...
                0b00000000,     // character 0xAA column 5 ........
          
                0b01001000,     // character 0xAB column 1 .*..*...
                0b00101000,     // character 0xAB column 2 ..*.*...
                0b00011000,     // character 0xAB column 3 ...**...
                0b01111100,     // character 0xAB column 4 .*****..
                0b00001000,     // character 0xAB column 5 ....*...
          
                0b00001000,     // character 0xAC column 1 ....*...
                0b01111100,     // character 0xAC column 2 .*****..
                0b00001000,     // character 0xAC column 3 ....*...
                0b00101000,     // character 0xAC column 4 ..*.*...
                0b00011000,     // character 0xAC column 5 ...**...
          
                0b01000000,     // character 0xAD column 1 .*......
                0b01001000,     // character 0xAD column 2 .*..*...
                0b01001000,     // character 0xAD column 3 .*..*...
                0b01111000,     // character 0xAD column 4 .****...
                0b01000000,     // character 0xAD column 5 .*......
          
                0b01010100,     // character 0xAE column 1 .*.*.*..
                0b01010100,     // character 0xAE column 2 .*.*.*..
                0b01010100,     // character 0xAE column 3 .*.*.*..
                0b01111100,     // character 0xAE column 4 .*****..
                0b00000000,     // character 0xAE column 5 ........
          
                0b00011000,     // character 0xAF column 1 ...**...
                0b00000000,     // character 0xAF column 2 ........
                0b01011000,     // character 0xAF column 3 .*.**...
                0b01000000,     // character 0xAF column 4 .*......
                0b00111000,     // character 0xAF column 5 ..***...

                0b00001000,     // character 0xB0 column 1 ....*...
                0b00001000,     // character 0xB0 column 2 ....*...
                0b00001000,     // character 0xB0 column 3 ....*...
                0b00001000,     // character 0xB0 column 4 ....*...
                0b00001000,     // character 0xB0 column 5 ....*...
          
                0b00000001,     // character 0xB1 column 1 .......*
                0b01000001,     // character 0xB1 column 2 .*.....*
                0b00111101,     // character 0xB1 column 3 ..****.*
                0b00001001,     // character 0xB1 column 4 ....*..*
                0b00000111,     // character 0xB1 column 5 .....***
          
                0b00100000,     // character 0xB2 column 1 ..*.....
                0b00010000,     // character 0xB2 column 2 ...*....
                0b11111000,     // character 0xB2 column 3 *****...
                0b00000100,     // character 0xB2 column 4 .....*..
                0b00000010,     // character 0xB2 column 5 ......*.
          
                0b00001110,     // character 0xB3 column 1 ....***.
                0b00000010,     // character 0xB3 column 2 ......*.
                0b01000011,     // character 0xB3 column 3 .*....**
                0b00100010,     // character 0xB3 column 4 ..*...*.
                0b00011110,     // character 0xB3 column 5 ...****.
          
                0b01000010,     // character 0xB4 column 1 .*....*.
                0b01000010,     // character 0xB4 column 2 .*....*.
                0b01111110,     // character 0xB4 column 3 .******.
                0b01000010,     // character 0xB4 column 4 .*....*.
                0b01000010,     // character 0xB4 column 5 .*....*.
          
                0b00100010,     // character 0xB5 column 1 ..*...*.
                0b00010010,     // character 0xB5 column 2 ...*..*.
                0b00001010,     // character 0xB5 column 3 ....*.*.
                0b01111111,     // character 0xB5 column 4 .*******
                0b00000010,     // character 0xB5 column 5 ......*.
          
                0b01000010,     // character 0xB6 column 1 .*....*.
                0b00111111,     // character 0xB6 column 2 ..******
                0b00000010,     // character 0xB6 column 3 ......*.
                0b01000010,     // character 0xB6 column 4 .*....*.
                0b00111110,     // character 0xB6 column 5 ..*****.
          
                0b00001010,     // character 0xB7 column 1 ....*.*.
                0b00001010,     // character 0xB7 column 2 ....*.*.
                0b01111111,     // character 0xB7 column 3 .*******
                0b00001010,     // character 0xB7 column 4 ....*.*.
                0b00001010,     // character 0xB7 column 5 ....*.*.
          
                0b00000100,     // character 0xB8 column 1 .....*..
                0b01000011,     // character 0xB8 column 2 .*....**
                0b01000001,     // character 0xB8 column 3 .*.....*
                0b00100001,     // character 0xB8 column 4 ..*....*
                0b00011111,     // character 0xB8 column 5 ...*****
          
                0b00000100,     // character 0xB9 column 1 .....*..
                0b00000011,     // character 0xB9 column 2 ......**
                0b01000010,     // character 0xB9 column 3 .*....*.
                0b00111110,     // character 0xB9 column 4 ..*****.
                0b00000010,     // character 0xB9 column 5 ......*.
          
                0b01000001,     // character 0xBA column 1 .*.....*
                0b01000001,     // character 0xBA column 2 .*.....*
                0b01000001,     // character 0xBA column 3 .*.....*
                0b01000001,     // character 0xBA column 4 .*.....*
                0b01111111,     // character 0xBA column 5 .*******
          
                0b00000010,     // character 0xBB column 1 ......*.
                0b01001111,     // character 0xBB column 2 .*..****
                0b00100010,     // character 0xBB column 3 ..*...*.
                0b00011111,     // character 0xBB column 4 ...*****
                0b00000010,     // character 0xBB column 5 ......*.
          
                0b01001010,     // character 0xBC column 1 .*..*.*.
                0b01001010,     // character 0xBC column 2 .*..*.*.
                0b01000000,     // character 0xBC column 3 .*......
                0b00100000,     // character 0xBC column 4 ..*.....
                0b00011100,     // character 0xBC column 5 ...***..
          
                0b01000010,     // character 0xBD column 1 .*....*.
                0b00100010,     // character 0xBD column 2 ..*...*.
                0b00010010,     // character 0xBD column 3 ...*..*.
                0b00101010,     // character 0xBD column 4 ..*.*.*.
                0b01000110,     // character 0xBD column 5 .*...**.
          
                0b00000010,     // character 0xBE column 1 ......*.
                0b00111111,     // character 0xBE column 2 ..******
                0b01000010,     // character 0xBE column 3 .*....*.
                0b01001010,     // character 0xBE column 4 .*..*.*.
                0b01000110,     // character 0xBE column 5 .*...**.
          
                0b00000011,     // character 0xBF column 1 ......**
                0b01000100,     // character 0xBF column 2 .*...*..
                0b01000000,     // character 0xBF column 3 .*......
                0b00100000,     // character 0xBF column 4 ..*.....
                0b00011110,     // character 0xBF column 5 ...****.

                0b00001000,     // character 0xC0 column 1 ....*...
                0b01000110,     // character 0xC0 column 2 .*...**.
                0b01001010,     // character 0xC0 column 3 .*..*.*.
                0b00110010,     // character 0xC0 column 4 ..**..*.
                0b00011110,     // character 0xC0 column 5 ...****.
          
                0b00001010,     // character 0xC1 column 1 ....*.*.
                0b01001010,     // character 0xC1 column 2 .*..*.*.
                0b00111110,     // character 0xC1 column 3 ..*****.
                0b00001001,     // character 0xC1 column 4 ....*..*
                0b00001000,     // character 0xC1 column 5 ....*...
          
                0b00001110,     // character 0xC2 column 1 ....***.
                0b00000000,     // character 0xC2 column 2 ........
                0b01001110,     // character 0xC2 column 3 .*..***.
                0b00100000,     // character 0xC2 column 4 ..*.....
                0b00011110,     // character 0xC2 column 5 ...****.
          
                0b00000100,     // character 0xC3 column 1 .....*..
                0b01000101,     // character 0xC3 column 2 .*...*.*
                0b00111101,     // character 0xC3 column 3 ..****.*
                0b00000101,     // character 0xC3 column 4 .....*.*
                0b00000100,     // character 0xC3 column 5 .....*..
          
                0b00000000,     // character 0xC4 column 1 ........
                0b01111111,     // character 0xC4 column 2 .*******
                0b00001000,     // character 0xC4 column 3 ....*...
                0b00010000,     // character 0xC4 column 4 ...*....
                0b00000000,     // character 0xC4 column 5 ........
          
                0b01000100,     // character 0xC5 column 1 .*...*..
                0b00100100,     // character 0xC5 column 2 ..*..*..
                0b00011111,     // character 0xC5 column 3 ...*****
                0b00000100,     // character 0xC5 column 4 .....*..
                0b00000100,     // character 0xC5 column 5 .....*..
          
                0b01000000,     // character 0xC6 column 1 .*......
                0b01000010,     // character 0xC6 column 2 .*....*.
                0b01000010,     // character 0xC6 column 3 .*....*.
                0b01000010,     // character 0xC6 column 4 .*....*.
                0b01000000,     // character 0xC6 column 5 .*......
          
                0b01000010,     // character 0xC7 column 1 .*....*.
                0b00101010,     // character 0xC7 column 2 ..*.*.*.
                0b00010010,     // character 0xC7 column 3 ...*..*.
                0b00101010,     // character 0xC7 column 4 ..*.*.*.
                0b00000110,     // character 0xC7 column 5 .....**.
          
                0b00100010,     // character 0xC8 column 1 ..*...*.
                0b00010010,     // character 0xC8 column 2 ...*..*.
                0b01111011,     // character 0xC8 column 3 .****.**
                0b00010110,     // character 0xC8 column 4 ...*.**.
                0b00100010,     // character 0xC8 column 5 ..*...*.
          
                0b00000000,     // character 0xC9 column 1 ........
                0b01000000,     // character 0xC9 column 2 .*......
                0b00100000,     // character 0xC9 column 3 ..*.....
                0b00011111,     // character 0xC9 column 4 ...*****
                0b00000000,     // character 0xC9 column 5 ........
          
                0b01111000,     // character 0xCA column 1 .****...
                0b00000000,     // character 0xCA column 2 ........
                0b00000010,     // character 0xCA column 3 ......*.
                0b00000100,     // character 0xCA column 4 .....*..
                0b01111000,     // character 0xCA column 5 .****...
          
                0b00111111,     // character 0xCB column 1 ..******
                0b01000100,     // character 0xCB column 2 .*...*..
                0b01000100,     // character 0xCB column 3 .*...*..
                0b01000100,     // character 0xCB column 4 .*...*..
                0b01000100,     // character 0xCB column 5 .*...*..
          
                0b00000010,     // character 0xCC column 1 ......*.
                0b01000010,     // character 0xCC column 2 .*....*.
                0b01000010,     // character 0xCC column 3 .*....*.
                0b00100010,     // character 0xCC column 4 ..*...*.
                0b00011110,     // character 0xCC column 5 ...****.
          
                0b00000100,     // character 0xCD column 1 .....*..
                0b00000010,     // character 0xCD column 2 ......*.
                0b00000100,     // character 0xCD column 3 .....*..
                0b00001000,     // character 0xCD column 4 ....*...
                0b00110000,     // character 0xCD column 5 ..**....
          
                0b00110010,     // character 0xCE column 1 ..**..*.
                0b00000010,     // character 0xCE column 2 ......*.
                0b01111111,     // character 0xCE column 3 .*******
                0b00000010,     // character 0xCE column 4 ......*.
                0b00110010,     // character 0xCE column 5 ..**..*.
          
                0b00000010,     // character 0xCF column 1 ......*.
                0b00010010,     // character 0xCF column 2 ...*..*.
                0b00100010,     // character 0xCF column 3 ..*...*.
                0b01010010,     // character 0xCF column 4 .*.*..*.
                0b00001110,     // character 0xCF column 5 ....***.

                0b00000000,     // character 0xD0 column 1 ........
                0b00101010,     // character 0xD0 column 2 ..*.*.*.
                0b00101010,     // character 0xD0 column 3 ..*.*.*.
                0b00101010,     // character 0xD0 column 4 ..*.*.*.
                0b01000000,     // character 0xD0 column 5 .*......
          
                0b00111000,     // character 0xD1 column 1 ..***...
                0b00100100,     // character 0xD1 column 2 ..*..*..
                0b00100010,     // character 0xD1 column 3 ..*...*.
                0b00100000,     // character 0xD1 column 4 ..*.....
                0b01110000,     // character 0xD1 column 5 .***....
          
                0b01000000,     // character 0xD2 column 1 .*......
                0b00101000,     // character 0xD2 column 2 ..*.*...
                0b00010000,     // character 0xD2 column 3 ...*....
                0b00101000,     // character 0xD2 column 4 ..*.*...
                0b00000110,     // character 0xD2 column 5 .....**.
          
                0b00000010,     // character 0xD3 column 1 ......*.
                0b00111110,     // character 0xD3 column 2 ..*****.
                0b01001010,     // character 0xD3 column 3 .*..*.*.
                0b01001010,     // character 0xD3 column 4 .*..*.*.
                0b01001010,     // character 0xD3 column 5 .*..*.*.
          
                0b00000100,     // character 0xD4 column 1 .....*..
                0b01111111,     // character 0xD4 column 2 .*******
                0b00000100,     // character 0xD4 column 3 .....*..
                0b00010100,     // character 0xD4 column 4 ...*.*..
                0b00001100,     // character 0xD4 column 5 ....**..
          
                0b01000000,     // character 0xD5 column 1 .*......
                0b01000010,     // character 0xD5 column 2 .*....*.
                0b01000010,     // character 0xD5 column 3 .*....*.
                0b01111110,     // character 0xD5 column 4 .******.
                0b01000000,     // character 0xD5 column 5 .*......
          
                0b01001010,     // character 0xD6 column 1 .*..*.*.
                0b01001010,     // character 0xD6 column 2 .*..*.*.
                0b01001010,     // character 0xD6 column 3 .*..*.*.
                0b01001010,     // character 0xD6 column 4 .*..*.*.
                0b01111110,     // character 0xD6 column 5 .******.
          
                0b00000100,     // character 0xD7 column 1 .....*..
                0b00000101,     // character 0xD7 column 2 .....*.*
                0b01000101,     // character 0xD7 column 3 .*...*.*
                0b00100101,     // character 0xD7 column 4 ..*..*.*
                0b00011100,     // character 0xD7 column 5 ...***..
          
                0b00001111,     // character 0xD8 column 1 ....****
                0b01000000,     // character 0xD8 column 2 .*......
                0b00100000,     // character 0xD8 column 3 ..*.....
                0b00011111,     // character 0xD8 column 4 ...*****
                0b00000000,     // character 0xD8 column 5 ........
          
                0b01111100,     // character 0xD9 column 1 .*****..
                0b00000000,     // character 0xD9 column 2 ........
                0b01111110,     // character 0xD9 column 3 .******.
                0b01000000,     // character 0xD9 column 4 .*......
                0b00110000,     // character 0xD9 column 5 ..**....
          
                0b01111110,     // character 0xDA column 1 .******.
                0b01000000,     // character 0xDA column 2 .*......
                0b00100000,     // character 0xDA column 3 ..*.....
                0b00010000,     // character 0xDA column 4 ...*....
                0b00001000,     // character 0xDA column 5 ....*...
          
                0b01111110,     // character 0xDB column 1 .******.
                0b01000010,     // character 0xDB column 2 .*....*.
                0b01000010,     // character 0xDB column 3 .*....*.
                0b01000010,     // character 0xDB column 4 .*....*.
                0b01111110,     // character 0xDB column 5 .******.
          
                0b00001110,     // character 0xDC column 1 ....***.
                0b00000010,     // character 0xDC column 2 ......*.
                0b01000010,     // character 0xDC column 3 .*....*.
                0b00100010,     // character 0xDC column 4 ..*...*.
                0b00011110,     // character 0xDC column 5 ...****.
          
                0b01000010,     // character 0xDD column 1 .*....*.
                0b01000010,     // character 0xDD column 2 .*....*.
                0b01000000,     // character 0xDD column 3 .*......
                0b00100000,     // character 0xDD column 4 ..*.....
                0b00011000,     // character 0xDD column 5 ...**...
          
                0b00000010,     // character 0xDE column 1 ......*.
                0b00000100,     // character 0xDE column 2 .....*..
                0b00000001,     // character 0xDE column 3 .......*
                0b00000010,     // character 0xDE column 4 ......*.
                0b00000000,     // character 0xDE column 5 ........
          
                0b00000111,     // character 0xDF column 1 .....***
                0b00000101,     // character 0xDF column 2 .....*.*
                0b00000111,     // character 0xDF column 3 .....***
                0b00000000,     // character 0xDF column 4 ........
                0b00000000,     // character 0xDF column 5 ........

                0b00111000,     // character 0xE0 column 1 ..***...
                0b01000100,     // character 0xE0 column 2 .*...*..
                0b01001000,     // character 0xE0 column 3 .*..*...
                0b00110000,     // character 0xE0 column 4 ..**....
                0b01000000,     // character 0xE0 column 5 .*......
          
                0b00100000,     // character 0xE1 column 1 ..*.....
                0b01010101,     // character 0xE1 column 2 .*.*.*.*
                0b01010100,     // character 0xE1 column 3 .*.*.*..
                0b01010101,     // character 0xE1 column 4 .*.*.*.*
                0b01111000,     // character 0xE1 column 5 .****...
          
                0b01111100,     // character 0xE2 column 1 .*****..
                0b00101010,     // character 0xE2 column 2 ..*.*.*.
                0b00101010,     // character 0xE2 column 3 ..*.*.*.
                0b00101010,     // character 0xE2 column 4 ..*.*.*.
                0b00010100,     // character 0xE2 column 5 ...*.*..
          
                0b00101000,     // character 0xE3 column 1 ..*.*...
                0b01010100,     // character 0xE3 column 2 .*.*.*..
                0b01010100,     // character 0xE3 column 3 .*.*.*..
                0b01000100,     // character 0xE3 column 4 .*...*..
                0b00100000,     // character 0xE3 column 5 ..*.....
          
                0b01111110,     // character 0xE4 column 1 .******.
                0b00010000,     // character 0xE4 column 2 ...*....
                0b00010000,     // character 0xE4 column 3 ...*....
                0b00001000,     // character 0xE4 column 4 ....*...
                0b00011110,     // character 0xE4 column 5 ...****.
          
                0b00111000,     // character 0xE5 column 1 ..***...
                0b01000100,     // character 0xE5 column 2 .*...*..
                0b01001100,     // character 0xE5 column 3 .*..**..
                0b01010100,     // character 0xE5 column 4 .*.*.*..
                0b00100100,     // character 0xE5 column 5 ..*..*..
          
                0b01111000,     // character 0xE6 column 1 .****...
                0b00010100,     // character 0xE6 column 2 ...*.*..
                0b00010010,     // character 0xE6 column 3 ...*..*.
                0b00010010,     // character 0xE6 column 4 ...*..*.
                0b00001100,     // character 0xE6 column 5 ....**..
          
                0b00001000,     // character 0xE7 column 1 ....*...
                0b01010100,     // character 0xE7 column 2 .*.*.*..
                0b01010100,     // character 0xE7 column 3 .*.*.*..
                0b01010100,     // character 0xE7 column 4 .*.*.*..
                0b00111100,     // character 0xE7 column 5 ..****..
          
                0b00100000,     // character 0xE8 column 1 ..*.....
                0b01000000,     // character 0xE8 column 2 .*......
                0b00111100,     // character 0xE8 column 3 ..****..
                0b00000100,     // character 0xE8 column 4 .....*..
                0b00000100,     // character 0xE8 column 5 .....*..
          
                0b00000010,     // character 0xE9 column 1 ......*.
                0b00000010,     // character 0xE9 column 2 ......*.
                0b00000000,     // character 0xE9 column 3 ........
                0b00000111,     // character 0xE9 column 4 .....***
                0b00000000,     // character 0xE9 column 5 ........
          
                0b00100000,     // character 0xEA column 1 ..*.....
                0b01000000,     // character 0xEA column 2 .*......
                0b01000100,     // character 0xEA column 3 .*...*..
                0b00111101,     // character 0xEA column 4 ..****.*
                0b00000000,     // character 0xEA column 5 ........
          
                0b00000101,     // character 0xEB column 1 .....*.*
                0b00000010,     // character 0xEB column 2 ......*.
                0b00000101,     // character 0xEB column 3 .....*.*
                0b00000000,     // character 0xEB column 4 ........
                0b00000000,     // character 0xEB column 5 ........
          
                0b00011000,     // character 0xEC column 1 ...**...
                0b00100100,     // character 0xEC column 2 ..*..*..
                0b01111110,     // character 0xEC column 3 .******.
                0b00100100,     // character 0xEC column 4 ..*..*..
                0b00010000,     // character 0xEC column 5 ...*....
          
                0b00010100,     // character 0xED column 1 ...*.*..
                0b01111111,     // character 0xED column 2 .*******
                0b01010100,     // character 0xED column 3 .*.*.*..
                0b01000000,     // character 0xED column 4 .*......
                0b01000000,     // character 0xED column 5 .*......
          
                0b01111100,     // character 0xEE column 1 .*****..
                0b00001001,     // character 0xEE column 2 ....*..*
                0b00000101,     // character 0xEE column 3 .....*.*
                0b00000101,     // character 0xEE column 4 .....*.*
                0b01111000,     // character 0xEE column 5 .****...
          
                0b00111000,     // character 0xEF column 1 ..***...
                0b01000101,     // character 0xEF column 2 .*...*.*
                0b01000100,     // character 0xEF column 3 .*...*..
                0b01000101,     // character 0xEF column 4 .*...*.*
                0b00111000,     // character 0xEF column 5 ..***...

                0b01111110,     // character 0xF0 column 1 .******.
                0b00010100,     // character 0xF0 column 2 ...*.*..
                0b00010010,     // character 0xF0 column 3 ...*..*.
                0b00010010,     // character 0xF0 column 4 ...*..*.
                0b00001100,     // character 0xF0 column 5 ....**..
          
                0b00001100,     // character 0xF1 column 1 ....**..
                0b00010010,     // character 0xF1 column 2 ...*..*.
                0b00010010,     // character 0xF1 column 3 ...*..*.
                0b00010100,     // character 0xF1 column 4 ...*.*..
                0b01111110,     // character 0xF1 column 5 .******.
          
                0b00111100,     // character 0xF2 column 1 ..****..
                0b01001010,     // character 0xF2 column 2 .*..*.*.
                0b01001010,     // character 0xF2 column 3 .*..*.*.
                0b01001010,     // character 0xF2 column 4 .*..*.*.
                0b00111100,     // character 0xF2 column 5 ..****..
          
                0b00110000,     // character 0xF3 column 1 ..**....
                0b00101000,     // character 0xF3 column 2 ..*.*...
                0b00010000,     // character 0xF3 column 3 ...*....
                0b00101000,     // character 0xF3 column 4 ..*.*...
                0b00011000,     // character 0xF3 column 5 ...**...
          
                0b01011000,     // character 0xF4 column 1 .*.**...
                0b01100100,     // character 0xF4 column 2 .**..*..
                0b00000100,     // character 0xF4 column 3 .....*..
                0b01100100,     // character 0xF4 column 4 .**..*..
                0b01011000,     // character 0xF4 column 5 .*.**...
          
                0b00111100,     // character 0xF5 column 1 ..****..
                0b01000001,     // character 0xF5 column 2 .*.....*
                0b01000000,     // character 0xF5 column 3 .*......
                0b00100001,     // character 0xF5 column 4 ..*....*
                0b01111100,     // character 0xF5 column 5 .*****..
          
                0b01100011,     // character 0xF6 column 1 .**...**
                0b01010101,     // character 0xF6 column 2 .*.*.*.*
                0b01001001,     // character 0xF6 column 3 .*..*..*
                0b01000001,     // character 0xF6 column 4 .*.....*
                0b01000001,     // character 0xF6 column 5 .*.....*
          
                0b01000100,     // character 0xF7 column 1 .*...*..
                0b00111100,     // character 0xF7 column 2 ..****..
                0b00000100,     // character 0xF7 column 3 .....*..
                0b01111100,     // character 0xF7 column 4 .*****..
                0b01000100,     // character 0xF7 column 5 .*...*..
          
                0b01000101,     // character 0xF8 column 1 .*...*.*
                0b00101001,     // character 0xF8 column 2 ..*.*..*
                0b00010001,     // character 0xF8 column 3 ...*...*
                0b00101001,     // character 0xF8 column 4 ..*.*..*
                0b01000101,     // character 0xF8 column 5 .*...*.*
          
                0b00001100,     // character 0xF9 column 1 ....**..
                0b01010000,     // character 0xF9 column 2 .*.*....
                0b01010000,     // character 0xF9 column 3 .*.*....
                0b0101000,     // character 0xF9 column 4 .*.*...
                0b00111100,     // character 0xF9 column 5 ..****..
          
                0b00001010,     // character 0xFA column 1 ....*.*.
                0b00001010,     // character 0xFA column 2 ....*.*.
                0b01111110,     // character 0xFA column 3 .******.
                0b00001010,     // character 0xFA column 4 ....*.*.
                0b00001001,     // character 0xFA column 5 ....*..*
          
                0b01000100,     // character 0xFB column 1 .*...*..
                0b00111100,     // character 0xFB column 2 ..****..
                0b00010100,     // character 0xFB column 3 ...*.*..
                0b00010100,     // character 0xFB column 4 ...*.*..
                0b01110100,     // character 0xFB column 5 .***.*..
          
                0b01111100,     // character 0xFC column 1 .*****..
                0b00010100,     // character 0xFC column 2 ...*.*..
                0b00011100,     // character 0xFC column 3 ...***..
                0b00010100,     // character 0xFC column 4 ...*.*..
                0b01111100,     // character 0xFC column 5 .*****..
          
                0b00001000,     // character 0xFD column 1 ....*...
                0b00001000,     // character 0xFD column 2 ....*...
                0b00101010,     // character 0xFD column 3 ..*.*.*.
                0b00001000,     // character 0xFD column 4 ....*...
                0b00001000,     // character 0xFD column 5 ....*...
          
                0b00000000,     // character 0xFE column 1 ........
                0b00000000,     // character 0xFE column 2 ........
                0b00000000,     // character 0xFE column 3 ........
                0b00000000,     // character 0xFE column 4 ........
                0b00000000,     // character 0xFE column 5 ........
          
                0b01111111,     // character 0xFF column 1 .*******
                0b01111111,     // character 0xFF column 2 .*******
                0b01111111,     // character 0xFF column 3 .*******
                0b01111111,     // character 0xFF column 4 .*******
                0b01111111      // character 0xFF column 5 .*******
                */

          default:          // '?'
              switch (dotcol)
              {
                 case 0: return 0b00000010; break;
                 case 1: return 0b00000001; break;
                 case 2: return 0b01010001; break;
                 case 3: return 0b00001001; break;
                 case 4: return 0b00000110; break;
              }
              break;
                           
          
         }
       return (0);
       }
