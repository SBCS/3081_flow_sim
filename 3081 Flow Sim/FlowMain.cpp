/*
 * DisplayMain.cpp
 *
 * Created: 6/21/2018 13:15:08
 *  Author: Fredc
 */ 

 #include "FlowMain.h"
 //#include "Test.h"

 //#include "NHD1286.h"
 #include "PCBA_3081.h"
 #include <stdio.h>

 //--------------------------------------------------------------------------------

 void FlowMain::FlowMain_Init (void)
    {
    Ser_Port = Serial_Port ( );
    Ser_Port.Serial1_init (9600);                       // Initialize
    Ser_Port.Send_Tx1 ("\r\nInitialize 9600 baud");
    PCBA_3081::Init ( );
    Flow_Output::Init ( );
    Last_Button_Pressed = NONE;
    Button_Armed = true;
    //NHD1286::Init ( );                  // Initialize display
    //aANSI.Init ( );                       // Initialize display
   // aANSI.Clear ( );                      // Clear of gibberish
   // aANSI.Put ("\033[0;0H");              // Set to row col = 0
    //aANSI.Put ("Init 9600 baud");         // Message
   // message_count = 0;
    }

 void FlowMain::FlowMain_Exec (void)
    {
    char ch;
    bool osf;             // one second flag
    bool tick_flag;
    double freq;
    char str [40];

   // Serial_Port::Send_Tx0 ("\r\n Check Point 2"); 
    
    Ser_Port.Tx1_check ( );                 // Send anything out
    ch = Ser_Port.Rx1_check ( );            // Check if any thing to do

    osf = PCBA_3081::Get_Timer_Flag ( );              // One second Timer
    tick_flag =  PCBA_3081::Get_Timer_Tick ( );


    if (tick_flag)
       {
       Flow_Output::Tick ( );
       Button_Pressed = Buttons::Read_Buttons ( );    // Get the button or NONE
       if (Button_Armed)                              // If the buttons are armed
          {
          if (Button_Pressed != NONE)                 // See if any pressed
             {
             Button_Hot = Button_Pressed;             // If so, capture
             Button_Armed = false;                    // And disarm
             }
          }
       switch (Button_Hot )                           // See if there is a hot button
          {
          case LEFT:
             freq = Flow_Output::Get_FreqA ( );
             freq = 2.0 * freq;
             Flow_Output::Set_FreqA (freq);
             sprintf (str,"\r\nA= %2.3f", freq);     // Erase screen
             Ser_Port.Send_Tx1 (str);
             //Flow_Output::FlowA (true);
             break;
          
          case UP:
             
             freq = Flow_Output::Get_FreqB ( );
             freq = 2.0 * freq;
             Flow_Output::Set_FreqB (freq);
             sprintf (str,"\r\nB= %2.3f", freq);            // Erase screen
             Ser_Port.Send_Tx1 (str);
             //Flow_Output::FlowB (true);
             break;

         case RIGHT:
             freq = Flow_Output::Get_FreqA ( );
             freq = 0.5 * freq;
             Flow_Output::Set_FreqA (freq);
             sprintf (str,"\r\nA= %2.3f", freq);            // Erase screen
             Ser_Port.Send_Tx1 (str);
             //Flow_Output::FlowA (false);
             break;
             
         case DOWN:
             
             freq = Flow_Output::Get_FreqB ( );
             freq = 0.5 * freq;
             Flow_Output::Set_FreqB (freq);
             sprintf (str,"\r\nB= %2.3f", freq);            // Erase screen
             Ser_Port.Send_Tx1 (str);
             //Flow_Output::FlowB (false);
             break;

          case MYSTERY:
             sprintf (str,"Mystery");            // Erase screen
             Ser_Port.Send_Tx1 (str);
             break;

          case OK:
             sprintf (str, "\r\nReset ");       // Blank line at bottom
             Ser_Port.Send_Tx1 (str); 
             Flow_Output::Init ( );
             break;
          case NONE:
             //Button_Hot = NONE;
             //Button_Armed = true;
             break;
          }
       Button_Hot = NONE;                       // Button has cooled
       //if (Last_Button_Pressed == NONE)
          //{
          //Button_Pressed = Buttons::Read_Buttons ( );
          //if ((Button_Pressed != NONE) && (Button_Hot == NONE))
             //{
             //Button_Hot = Button_Pressed;
             //Last_Button_Pressed = Button_Pressed;
             //}

       }

    if (osf)
       {
       osf_count += 1;
      // Buttons::Test_Buttons (osf_count);
       if (osf_count >= 2) osf_count = 1;
       if ((!Button_Armed) && (Button_Pressed == NONE))
         {
         Button_Armed = true;
         }
       }
     //  }
    //switch (ch) 
       //{                                    // test aANSI.State to go around these numbers
       //case '\0':                 break;
       //default: 
          //Ser_Port.Send_Tx0 (ch);            // Send to serial port
          //break;
       //}

    switch (ch)                  // Send to display
       {
     //  case '~':  aANSI.Put (ESC); break;  // escape proxy Termite will forward escapes
       //case '\r':                 break;   // Skip these for trouble shoot
       //case '\n':                 break;
       //case '\t':                 break;
       case '\0':                 break;
      // default:  aANSI.Put (ch);  break;
       }

    if (osf)            //. Timer flag outputs
       {
      // Ser_Port.Send_Tx1 ("\r\nX");

       //switch (Button_Pressed)
          //{
          //case MYSTERY:
             ////sprintf (str,"~[2J");            // Erase screen
             ////Ser_Port.Send (str);
             //break;
          //case OK:
             //sprintf (str, "123456789 ");       // Blank line at bottom
             //Ser_Port.Send_Tx1 (str);
             //break;
          //}

       //message_count += 1;
       //if (message_count > 20) message_count = 20;
       //switch (message_count)                                        // Trot the messages out 1 by 1
          //{
          //case 1: Ser_Port.Send_Tx0 ("\r\n ~1 Test1"); break;
          //case 2: Ser_Port.Send_Tx0 ("\r\n ~2 Test2"); break;
          //case 3: Ser_Port.Send_Tx0 ("\r\n ~3 Test3"); break;
          //case 4: Ser_Port.Send_Tx0 ("\r\n ~4 Test4"); break;
          //case 5: Ser_Port.Send_Tx0 ("\r\n ~5 Test5"); break;
          //case 6: Ser_Port.Send_Tx0 ("\r\n ~6 Test6"); break;
          //case 7: Ser_Port.Send_Tx0 ("\r\n ~c Clear"); break;
          //case 1: aANSI.Put ("\r\n ~ 1 Test1"); break;            // Put space after ~ so test isn't triggered
          //case 2: aANSI.Put ("\r\n ~ 2 Test2"); break;
          //case 3: aANSI.Put ("\r\n ~ 3 Test3"); break;
          //case 4: aANSI.Put ("\r\n ~ 4 Test4"); break;
          //case 5: aANSI.Put ("\r\n ~ 5 Test5"); break;
          //case 6: aANSI.Put ("\r\n ~ 6 Test6"); break;
          //case 7: aANSI.Put ("\r\n ~ c Clear"); break;
                 //case 'c':  aANSI.Clear ( ); break;    //Serial_Port::Send_Tx0 (ch);
       //   }

       }

    }

 