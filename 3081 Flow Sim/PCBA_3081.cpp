  /*
  * PCBA_3081.cpp
  *
  * Created: 5/25/2017 15:11:31
  *  Author: Fredc
  */ 
  #include <avr/io.h>
  #include <avr/interrupt.h>
  #define F_CPU 8000000
  #include <util/delay.h>
  #include <avr/wdt.h>
  //#include "Config.h"

  #include "util.h"
  #include "PCBA_3081.h"

  #define INITIAL_FLOW_FREQ_A 32.0  
  #define INITIAL_FLOW_FREQ_B 32.0


  static int  OneSecondTimer;        // 5ms down counter for tracking 1 second intervals
  //static bool OneSecondFlag;       // 1 = 10 sec over
  //static int  HalfSecondTimer;     // 5ms down counter for tracking 1 second intervals
  //static bool HalfSecondFlag;      // 1 = 10 sec over
  static volatile bool Timer_Flag;            // 1 second flag
  static volatile bool Timer_Tick;            // 2 ms Tick
  
  
   
  //---------------------------------------------------
 
  //----------------------
 

  #define PUD_BIT   0x40       // Pull up disable
  void PCBA_3081::Init ( )
     {
     //DDRA = 0x80;
     DDRC = 0x0c;       // Button rows, col 3, 4
     DDRB |= 0;         // Button Col 6
     PORTC = 0x0f;      // Set col pullups and rows hi    
     PORTB |= 1;        //

     DDRD |= 0x60;      // Flow outputs
     }
  
 
    void PCBA_3081::Test (int t)
       {
       // Stub to inset test code
       }
    //=============================================================
   

  //=================Flow Outputs===================================
  static volatile int FlowA_Count;
  static volatile int FlowB_Count;
  static volatile double FlowA_Freq;
  static volatile double FlowB_Freq;
  static volatile bool FlowA_Out;
  static volatile bool FlowB_Out;


  void Flow_Output::Init (void)
     {
     FlowA_Count = 500;
     FlowB_Count = 500;
     FlowA_Freq = INITIAL_FLOW_FREQ_A;
     FlowB_Freq = INITIAL_FLOW_FREQ_B;
     FlowA_Out = false;
     FlowB_Out = false;
     }

  void Flow_Output::Tick (void)        // Strobe flow
     {
     FlowA_Count -= 1;
     if (FlowA_Count == 0)
         {
         FlowA_Out = !FlowA_Out;
         FlowA (FlowA_Out);
         FlowA_Count = (int) (250.0 / FlowA_Freq);
         }

    FlowB_Count -= 1;
    if (FlowB_Count == 0)
       {
       FlowB_Out = !FlowB_Out;
       FlowB (FlowB_Out);
       FlowB_Count = (int) (250.0 / FlowB_Freq);
       }
    }

  void Flow_Output::Set_FreqA (double freq)
     {
     FlowA_Freq = freq;
     }

  void Flow_Output::Set_FreqB (double freq)
     {
     FlowB_Freq = freq;
     }

 double Flow_Output::Get_FreqA (void)
    {
    return FlowA_Freq;
    }

 double Flow_Output::Get_FreqB (void)
    {
    return FlowB_Freq;
    }

  //----------------------------------
  void Flow_Output::FlowA (bool A)
     {
     if (A) PORTD |= 0x20;
        else PORTD &= ~0x20;
     }

  void Flow_Output::FlowB (bool B)
    {
    if (B) PORTD |= 0x40;
         else PORTD &= ~0x40;
    }

  //===================Buttons=====================================
  #define col3 0x02
  #define col4 0x01
  #define col6 0x01
  int row1 = 0xff;
  int row2 = 0xff;
  int row1b = 0xff;
  int row2b = 0xff;

  Button Buttons::Read_Buttons ( )
     {
     //int row1;
     //int row2;

     PORTC &= ~0x08;    // Do row 1
     _delay_us (100);   //
     row1 = PINC;
     row1b = PINB;      // Read column 6
     PORTC |= 0x08;

     PORTC &= ~0x04;    // Do row 2
     _delay_us (100);   //
     row2 = PINC;
     row2b = PINB;      // Read column 6
     PORTC |= 0x04;


     if ((row1 & col3) == 0) return RIGHT;   //Button.
     if ((row2 & col3) == 0) return DOWN;    //Button.
     if ((row1 & col4) == 0) return UP;      //Button.
     if ((row2 & col4) == 0) return LEFT;    //Button.
     if ((row1b & col6) == 0) return OK;      //Button.
     if ((row2b & col6) == 0) return MYSTERY; //Button.
     return NONE;                             //Button.MYSTERY;
  }
  
  Button Buttons::Test_Buttons (int test)
     {
     switch (test)
        {
        case 0:
            PORTB |= 0x01;    // Do row 1
           break;
        case 1:
            PORTB &= ~0x01;    // Do row 1
           break;
       default:   
          break;
        }
     return OK;
     }
  
   
//-----------------------------------Watch Dog----------------------------------------------
//
void PCBA_3081::WatchDog_Init (void)
   {
    
   WatchDog_Off ( );       // Turn off WDT
    return;
       
 //  MCUSR = 0;        // Tiny p46
   WDTCR |= 0x69;
   //wdt_enable (15);
   //wdt_enable (WDTO_8S);
   }


void PCBA_3081::WatchDog_Touch (void)
   {
   // WDTCR |= 0x39;
   wdt_reset ( );
   //WDTCR |= 0x1f;
   }

void PCBA_3081::WatchDog_Off  (void)
   {
   //wdt_disable ( );
   //WDTCR |= (1 << WDCE) | (1 << WDE);     // Enable turn off
   //WDTCR = 0x00;                         // Turn off WDT
   
  // MCUSR &= 0x08; //_WDR();/* Clear WDRF in MCUSR */ 
 //  MCUSR = 0x00;            /* Write logical one to WDCE and WDE */
   WDTCR |= (1<<WDCE) | (1<<WDE);/* Turn off WDT */
   WDTCR = 0x00;
   }   

void PCBA_3081::Reboot  (void)
   {
   ((void (*)(void))0)();  
   }

//ISR (WDT_vect)
//  {
 // PCBA201705::WatchDog_Off ( );
//  PCBA201705::Reboot ( );
 // }  

 //=========================================Timer============================ 

 bool PCBA_3081::Get_Timer_Flag (void)
    {
    if (Timer_Flag)           // Flag is up
       {
       Timer_Flag = false;    // Lower
       return true;           //
       }
    return false;
    }

 bool PCBA_3081::Get_Timer_Tick (void)
 {
    if (Timer_Tick)
    {
       Timer_Tick = false;
       return true;
    }
    return false;
 }
 

 #define ONESECONDTIME       500    /* Number of ticks for 1 sec    */
// #define HALFSECONDTIME      250    /* Number of ticks for 1/2 sec  */
 //#define TIMER0RELOAD        193    /* for 1 ms timer overflow 193*/
 #define TIMER0RELOAD        241    /* for 2 ms timer overflow 193*/
 
 void PCBA_3081::Timer_Init (void)
    {
    //-------Initialize Timer-------------------------------
    cli();
    TCCR0 = 0x05;                  // 8 bit, Pre = 1024 
    TIMSK |= 2;                    // enable timer overflow interrupt
     
    TCNT0 = TIMER0RELOAD;
    Timer_Flag = false;
    Timer_Tick = false;
    OneSecondTimer = ONESECONDTIME;          // set 200*5ms for the second timer
    //HalfSecondTimer = HALFSECONDTIME;
    }

 //-----------------------------------------------------------------------------------
 ISR (TIMER0_OVF_vect)                       // Overflow detect
    {
    
    TCNT0 = TIMER0RELOAD;

    Timer_Tick = true;                       //
    OneSecondTimer--;                        // Decrement the one second timer
    if (OneSecondTimer == 0)                 // If the second timer has run out ...
       {
   
       OneSecondTimer = ONESECONDTIME;       // Reload the one second timer
       Timer_Flag = true;                    // Show it has been one second for the check
       }
    if (OneSecondTimer < 0) OneSecondTimer = ONESECONDTIME;
    }
