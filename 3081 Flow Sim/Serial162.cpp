/*
 * Serial162.cpp
 *
 * Created: 6/15/2018 08:24:07
 *  Author: Fredc
 */ 
#include <avr/io.h>
#include <avr/cpufunc.h>
#include <avr/interrupt.h>
//#include <string.h>

#include "Serial162.h"
#include "Util.h"

//----------------------------------------------------------------

Box Tx0 = Box ( );
Box Rx0 = Box ( );      // Interrupt driven buffer

Box Tx1 = Box ( );
Box Rx1 = Box ( );      // Interrupt driven buffer

//#define RxBuff0_SIZE  60
//#define RxBuff1_SIZE  60

//char Rx_Buff0 [RxBuff0_SIZE];    // primary input buffer
//char Rx_Line0 [RxBuff0_SIZE];    // Line buffer
//int  Rx_Index0;

//char Rx_Buff1 [RxBuff1_SIZE];    // primary input buffer
//char Rx_Line1 [RxBuff1_SIZE];    // Line buffer
//int  Rx_Index1;

void Serial_Port::Init (int Baud_Rate)
   {
   UART0_init (Baud_Rate);
   }


void Serial_Port::Serial1_init (int Baud_Rate)
   {
   UART1_init (Baud_Rate);
   }

//------------------------------------------------------------------------
// Assembles characters into line. Returns true when ready
//
//bool Serial_Port::Line0_Ready ( )
//{
   //char c;
   //
   //c = Rx0_check ( );                     // Get a potential character
   //if (c != '\0')                         // If not NULL then inspect
   //{                                   //
      //if (c == '\r')                      // If CR then terminate
      //{                                //
         //Rx_Buff0 [Rx_Index0] = '\0';     //
         //return true;                     // And scram
      //}                                //
      //if (c != '\n')                      // Toss away these
      //{
         //Rx_Buff0 [Rx_Index0] = c;        // Else poke into buffer
         //Rx_Index0 += 1;                  // Jack the index
         //if (Rx_Index0 > RxBuff0_SIZE)    // Did we wrap?
         //Rx_Index0 = 0;                // Yes, so just start over
      //}
   //}
   //return false;
//}
//

//bool Serial_Port::Line1_Ready ( )
//{
   //char c;
   //
   //c = Rx1_check ( );                     // Get a potential character
   //if (c != '\0')                         // If not NULL then inspect
   //{                                   //
      //if (c == '\r')                      // If CR then terminate
      //{                                //
         //Rx_Buff1 [Rx_Index1] = '\0';     //
         //return true;                     // And scram
      //}                                //
      //if (c != '\n')                      // Toss away these
      //{
         //Rx_Buff1 [Rx_Index1] = c;        // Else poke into buffer
         //Rx_Index1 += 1;                  // Jack the index
         //if (Rx_Index1 > RxBuff1_SIZE)    // Did we wrap?
         //Rx_Index1 = 0;                // Yes, so just start over
      //}
   //}
   //return false;
//}

//-----------------------------------------
//char *Serial_Port::Get_Line0 ( )          // Returns a formed line.
   //{
   ////  strcpy (Rx_Line1, Rx_Buff1);       // Copy our line
   //char *ss;
   //char *dd;
//
//
   //dd = Rx_Line0;       // Destination
   //ss = Rx_Buff0;       // Source
//
   //while (*dd++ = *ss++)
      //;
   //Rx_Index0 = 0;                         // Reset buffer index
   //return Rx_Line0;
//}

//---------------------------------------

//char *Serial_Port::Get_Line1 ( )          // Returns a formed line.
//{
   ////  strcpy (Rx_Line1, Rx_Buff1);       // Copy our line
   //char *ss;
   //char *dd;
//
//
   //dd = Rx_Line1;       // Destination
   //ss = Rx_Buff1;       // Source
//
   //while (*dd++ = *ss++)
   //;
   //Rx_Index1 = 0;                         // Reset buffer index
   //return Rx_Line1;
//}


//-------------------------------------------------------------------------
void Serial_Port::Send_Tx0 (char ch)      // Send a char
   {
   Tx0.Put_char (ch);                     // Stuff to Buff
  // UART0_Transmit (ch);
   }
   
void Serial_Port::Send_Tx1 (char ch)     // Send a char
   {
   Tx1.Put_char (ch);
   //UART1_Transmit (ch);
   }


void Serial_Port::Tx0_check ( )              // Checks Tx buffer and sends any
   {   
   char ch;
if ((UCSR0A & 0x20) != 0)              // Is the out buffer ready?
   if (Tx0.Ready ( ))                  // Char avail?  
      {
      ch = Tx0.Get_char ( );             // Get it
          UART0_Transmit (ch);
        // ANSI0.Put (ch);
      } 
   


   //if (Tx0.Ready ( ))                          // Char avail?    //Go to ANSI here
      //if ((UCSR0A & 0x20) != 0)                // Then send it
         //{
         //UART0_Transmit (Tx0.Get_char ( ));
         //}
   
   }

void Serial_Port::Tx1_check ( )              // Checks Tx buffer and sends any
   {

   if (Tx1.Ready ( ))                          // Char avail?
   if ((UCSR1A & 0x20) != 0)                     // Then send it
      {
      UART1_Transmit (Tx1.Get_char ( ));
      }

   }
//-------------------------------------------------------------------

char Serial_Port::Rx0_check ( )              // Checks Rx
   {
   char ch;

   ch = '\0';                                // Set equal to null
   if (Rx0.Ready ( ))                        // See  if a character came in
      ch = Rx0.Get_char ( );                 // If so get it
   return ch;                                // And return
   }

char Serial_Port::Rx1_check ( )              // Checks Rx
   {
   char ch;

   ch = '\0';                                // Set equal to null
   if (Rx1.Ready ( ))                        // See  if a character came in
      ch = Rx1.Get_char ( );                 // If so get it
   return ch;                                // And return
   }

void Serial_Port::Send_Tx0 (char *ptr)       // Sends a line of data
   {
   while (*ptr)
      {
      Tx0.Put_char (*ptr++);
      }
   //Tx0.Put_char ('\0');
   }   

void Serial_Port::Send_Tx1 (char *ptr)       // Sends a line of data
   {
   while (*ptr)
      {
      Tx1.Put_char (*ptr++);
      }
   //Tx1.Put_char ('\0');
   }   

//-------------------------------------------
void Serial_Port::UART0_init (int baud_rate)
   {
   int UBRR;
   
   UBRR = 103; // Default 9600
   switch (baud_rate)
      {
      case 2400:   UBRR = 416; break;        // Need to set these up for UART0
      case 4800:   UBRR = 207; break;        // Not the same as UART1 :(
      case 9600:   UBRR = 103; break;
      case 14400:  UBRR =  68; break;
      case 19200:  UBRR =  51; break;
      case 28800:  UBRR =  34; break;
      //case 115200: UBRR =   8; break;
      }

   UBRR0H = UBRR >> 8;     // Set baud rate
   UBRR0L = UBRR;          //

   UCSR0A = 2;             // U2X set to 1 ( doubling)
   UCSR0C = 0x86;          // Set frame format: 8N1
   UCSR0B = 0x98;          // Enable receiver and transmitter
   }


void Serial_Port::UART1_init (int baud_rate)
   {
   int UBRR;

   UBRR = 103; // Default 9600
   switch (baud_rate)
      {
      case 2400:   UBRR = 416; break;        // Need to set these up for UART0
      case 4800:   UBRR = 207; break;        // Not the same as UART1 :(
      case 9600:   UBRR = 103; break;
      case 14400:  UBRR =  68; break;
      case 19200:  UBRR =  51; break;
      case 28800:  UBRR =  34; break;
      //case 115200: UBRR =   8; break;
      }      
   UBRR1H = UBRR >> 8;
   UBRR1L = UBRR;
   
   UCSR1A = 2;          // U2X set to 0 (no doubling)
   UCSR1C = 0x86;       // Set frame format: 8N1
   UCSR1B = 0x98;       // Enable receiver and transmitter
   }

//-----------------------------------------------------
//void Serial_Port::UART0_Transmit (char data)
   //{
   //while (!(UCSR0A & 0x20));     // Wait for empty transmit buffer
   //UDR0 = data;                  // Put data into buffer, sends the data
   //}
//
//unsigned char Serial_Port::UART0_Receive (void)
   //{
   //
   //while (!(UCSR0A & 0x80));     // Wait for data to be received
   ////{for (i=0;i<50000;i++);}
   //// Get and return received data from buffer
   //return UDR0;
   //}




//---------------------------------------------------------



void Serial_Port::UART1_Transmit (char data)
   {
   /* Wait for empty transmit buffer */
   while (!(UCSR1A & 0x20))
      ;
   /* Put data into buffer, sends the data */
   UDR1 = data;
   }

unsigned char Serial_Port::UART1_Receive (void)
   {
   /* Wait for data to be received */
   while (!(UCSR1A & 0x80))
      ;
   //{for (i=0;i<50000;i++);}
   /* Get and return received data from buffer */
   return UDR1;
   }




//-----------------------------------------------------------

ISR (USART0_RXC_vect)
   {
   while ((UCSR0A & 0x80) == 0)
      ;
   Rx0.Put_char (UDR0);    // Read and clear interrupt
   }

ISR (USART1_RXC_vect)
{
   while (!(UCSR1A & 0x80))
      ;
   //{for (i=0;i<50000;i++);}
   // Get and return received data from buffer
   // return  UDR1;
   Rx1.Put_char (UDR1);    // Read and clear interrupt
}

//ISR (USART1_TX_vect)
//   {

//   }

