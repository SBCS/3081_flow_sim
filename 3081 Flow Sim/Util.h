/*
 * Util.h
 *
 * Created: 6/2/2017 11:51:31
 *  Author: Fredc
 */ 


#ifndef UTIL_H_
#define UTIL_H_
//---------------------------------------------------------------
//
// Standard bit definitions
//
#define BIT_0					0x01
#define BIT_1					0x02
#define BIT_2					0x04
#define BIT_3					0x08
#define BIT_4					0x10
#define BIT_5					0x20
#define BIT_6					0x40
#define BIT_7					0x80

class Utilitas
   {
   public:
      int    Bracket (int x, int high, int low);
      double Bracket (double x, double high, double low);
      double Absolute (double x);
      char*  itoa (int num, char* str);
      char*  pHtoa (int num, char* str);

   protected:

   private:

   };

#define BOX_ENTRIES 50

class Box
{
   public:
   Box ( );
   ~Box (void);
   
   int  Put_char (char val);           // Returns incounter
   char Get_char ( );                  // Returns byte

   bool Ready (void);                  // Returns data ready flag

   private:
   char message [BOX_ENTRIES];
   int incounter;
   int outcounter;
   int buffersize;
   
   int  Next_In  (void);               // Returns incounter
   int  Next_Out (void);               // Returns outcounter
};

#endif /* UTIL_H_ */