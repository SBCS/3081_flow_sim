/*
 * EEProm.h
 *
 * Created: 5/25/2017 15:15:54
 *  Author: Fredc
 */ 

#include "config.h"

#ifndef EEPROM_H_
#define EEPROM_H_


class EEProm
   {

   /*******************************************************************************
   *
   * EEProm.h
   *
   * Contains routines that deal with EEProm
   *

   *******************************************************************************/

   public:

      void    EEProm_Init (void);
     
      void    Save_Resist_Cal (int unit, int Resist_Cal);    
      int     Get_Resist_Cal  (int unit);

      void   Save_Temp_Cal (int unit, int Temp_Cal);
      int    Get_Temp_Cal  (int unit);
      
      void   WriteWordEE (int address, int data);
      
      int    ReadWordEE  (int address);
      void   WriteLongEE (int address, long data);
       
   private:
      double tempf;          // Work area
      char   tempc [4];
      
      void   EEProm_Set_Default (void);
      
      char   ReadByteEE  (int address);
      void   WriteByteEE (int address, unsigned char data);
      
      long    ReadLongEE  (int address);
      //void   WriteLongEE (int address, int data)
      void   WriteDoubleEE (int address, double data);
      double ReadDoubleEE  (int address);
      
      //void   Save_Part_String  (int sw, char *st);     // For test
   };



#endif /* EEPROM_H_ */