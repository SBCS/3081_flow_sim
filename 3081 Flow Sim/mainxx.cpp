/*
 * 3069 Probe Sim Display.cpp
 *
 * Created: 6/14/2018 12:39:38
 * Author : Fredc
 */ 
 #define F_CPU  8000000UL
#include <avr/io.h>
#include <avr/interrupt.h>

#include "DisplayMain.h"
#include "PCBA2018.h"
//#include "NHD1286.h"
//#include "Serial162.h"




int main(void)
   {
   DisplayMain DispMain;
   bool T_Flag;

   DispMain = DisplayMain ( );

   PCBA2018::Timer_Init ( );
   DispMain.DispMain_Init ( );

   sei ( );

   
    while (1) 
      {
      T_Flag = PCBA2018::Get_Timer_Flag ( );
      DispMain.DispMain_Exec (T_Flag);                  // Execute main line
      if (T_Flag)
         { 
         //Serial_Port::Send_Tx0 ("\r\n Check Point 1"); 
         }         
      }         
   return 0;   /* never reached */
                 
   }// end main
  

