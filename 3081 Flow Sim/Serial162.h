/*
 * Serial162.h
 *
 * Created: 6/15/2018 08:24:26
 *  Author: Fredc
 */ 


#ifndef SERIAL162_H_
#define SERIAL162_H_

//#include "ANSI.h"

#ifndef  __AVR_ATmega162__
#error  Not ATMega162
#endif

class Serial_Port
   {
   public:
      void Init (int Baud_Rate);
      void Send_Tx0 (char *ptr);   // Send a line
      void Send_Tx0 (char ch);     // Send a char
      void Tx0_check ( );
      char Rx0_check ( );          // Returns next char or null

      //static bool Line0_Ready ( );         // Assembles characters into line. Returns true when ready
      //static char *Get_Line0 ( );          // Returns formed line.
      
      void Serial1_init (int Baud_Rate);
      void Send_Tx1 (char *ptr);   // Send a line
      void Send_Tx1 (char ch);     // Send a char
      void Tx1_check ( );
      char Rx1_check ( );          // Returns next char or null

      //static bool Line1_Ready ( );         // Assembles characters into line. Returns true when ready
      //static char *Get_Line1 ( );          // Returns formed line.

   protected:
   
   private:
      void UART0_init (int baud_rate);
      void UART0_Transmit (char data);
      //ANSI ANSI0;
      unsigned char UART0_Receive (void);
     
      void UART1_init (int baud_rate); 
      void UART1_Transmit (char data);
      //ANSI ANSI1;
      unsigned char UART1_Receive (void);
   };








#endif /* SERIAL162_H_ */