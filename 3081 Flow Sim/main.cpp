/*
 * 3081 Flow Sim.cpp
 *
 * Created: 10/22/2018 08:56:06
 * Author : Fredc
 */ 

 #define F_CPU  8000000UL
 #include <avr/io.h>
 #include <avr/interrupt.h>

 #include "FlowMain.h"
 #include "PCBA_3081.h"

#include "FlowMain.h"




int main(void)
   {
   FlowMain Flow_Main;
   //bool T_Flag;

   //FlowMain = Flow_Main ( );

   PCBA_3081::Timer_Init ( );
   Flow_Main.FlowMain_Init ( );

   sei ( );

   
   while (1)
      {
      //T_Flag = PCBA_3081::Get_Timer_Flag ( );
      Flow_Main.FlowMain_Exec ( );                  // Execute main line
       //if (T_Flag)
         //{
        // //Serial_Port::Send_Tx0 ("\r\n Check Point 1");
        // }
      }
   return 0;   /* never reached */
   
}// end main
