/*
 * Config.h
 *
 * Created: 5/25/2017 15:14:22
 *  Author: Fredc
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_
#include <stdint.h>



#define  FIRMWARE_MAJOR_REV 2        // Firmware revision
#define  FIRMWARE_MINOR_REV 1            // Firmware revision




// PCBA Codes -----
#define PCBA_201705				1		   // attiny461, XFR Latching
#define PCBA_2018             2        // New Haven Display Module

//#ifdef __AVR_ATmega88PA__
//#ifdef __AVR_ATtiny461__
//#ifdef __AVR_AT90CAN32__
//#ifdef __AVR_ATmega88__

//---------------------------------------------------------------
//
// Standard bit definitions
//
#define BIT_0					0x01											
#define BIT_1					0x02									
#define BIT_2					0x04									
#define BIT_3					0x08											
#define BIT_4					0x10											
#define BIT_5					0x20											
#define BIT_6					0x40											
#define BIT_7					0x80									

#define byte uint8_t


//-----------------------------------------------------------------
// Data types
//
//typedef uint8_t byte;

//typedef uint8_t byte;
//typedef uint8_t bool;
/*
 (signed/unsigned) char - 1 byte
 (signed/unsigned) short - 2 bytes
 (signed/unsigned) int - 2 bytes
 (signed/unsigned) long - 4 bytes
 (signed/unsigned) long long - 8 bytes
 float - 4 bytes (floating point)
 double - alias to float


 C99 standard available to GCC. 
 These use the convention of a "u" to denote the signedness (no "u" to denote signed), 
 "int" to denote that it's a integer and not a float, 
 the number of bits in the int and a trailing "_t". 
 Examples:


 Code:
 int8_t - Signed Char
 uint16_t - Unsigned Int
 uint32_t - Unsigned Long
 int64_t - Signed Long Long

 */
 
#endif /* CONFIG_H_ */