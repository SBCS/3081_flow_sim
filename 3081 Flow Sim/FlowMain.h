/*
 * DisplayMain.h
 *
 * Created: 6/21/2018 13:14:43
 *  Author: Fredc
 */ 


#ifndef FLOWMAIN_H_
#define FLOWMAIN_H_

//#include "ANSI.h";
#include "Util.h"
#include "Serial162.h"
#include "PCBA_3081.h"
class FlowMain
   {
   public:
     void FlowMain_Init (void);
     void FlowMain_Exec (void);

   protected:

   private:
     Serial_Port Ser_Port; 
     Utilitas    Util;

     //int    message_count;
     int    osf_count;

     bool   Button_Armed;
     Button Button_Hot;
     bool   Button_Action_Done;
     Button Last_Button_Pressed;
     Button Button_Pressed;

   };


#endif /* FLOWMAIN_H_ */