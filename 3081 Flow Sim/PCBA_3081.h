/*
 * PCBA_3081.h
 *   All these routines are specific to this PCB
 *
 * Created: 5/25/2017 15:11:53
 *  Author: Fredc
 */ 


#ifndef PCBA_3081_H_
#define PCBA_3081_H_

  
class PCBA_3081 // :  public IO

   {
   public:
      static void Init (void);                        // Initialize IO 
      
      static void Timer_Init (void);                  // Initialize timer
      static bool Get_Timer_Flag (void);              // One second Timer
      static bool Get_Timer_Tick (void);              // 2 ms Tick
      
      static void WatchDog_Init  (void);
      static void WatchDog_Touch (void);
      static void WatchDog_Off   (void);
     
      static void Test (int t);                       // A test
	  
      static void Reboot  (void);
    
   protected: 

   private:
   
   } ;
  //--------------------------------------------------------
  enum Button {NONE, UP, DOWN, LEFT, RIGHT, OK, MYSTERY};
  class Buttons
     {
     public:
        static Button Read_Buttons ( );
        static Button Test_Buttons (int test);
     protected:

     private:

     };

  //-------------------------------------------------------------------

  class Flow_Output
     {
     public:
        static void Init (void);
        static void Tick (void);        // Strobe flow 
        static void Set_FreqA (double freq);    // Set freq Hz
        static void Set_FreqB (double freq);    //
        static double Get_FreqA (void);    // Set freq Hz
        static double Get_FreqB (void);    //

        static void FlowA (bool A);          // Set output
        static void FlowB (bool B);

     protected:

     private:
        //static int FlowA_Count;
        //static int FlowB_Count;
        //static int FlowA_Freq;
        //static int FlowB_Freq;
        //static bool FlowA_Out;
        //static bool FlowB_Out;
     };
  //--------------------------------------------------------------------------------------------
    




#endif /* PCBA201705_H_ */